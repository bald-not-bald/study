#define  _CRT_SECURE_NO_WARNINGS 1

#include<stdio.h>
#include<stdlib.h>
#include<string.h>


//柔性数组
//c99中，结构中最后一个元素允许是未知大小的数组，这个就叫做柔性数组
//typedef struct S
//{
//	int n;
//	char c;
//	char arr[];//数组的大小是未知
//}S;
//
//int main()
//{
//	S* ps = (S*)malloc(sizeof(struct S) + 10 * sizeof(char));
//	ps->n = 10;
//	int i = 0;
//	for (i = 0; i < 10; i++)
//	{
//		ps->arr[i] = 'Q';
//	}
//	//增容
//	S* ptr = (S*)realloc(ps, sizeof(S*) + 20 * sizeof(char));
//	if (ps != NULL)
//	{
//		ps = ptr;
//	}
//	else
//	{
//		perror("realloc\n");
//		return 1;
//	}
//	for (i = 0; i < 10; i++)
//	{
//		printf("%\n", ps->arr[i]);
//	}
//	printf("%zd\n", sizeof(S));
//	free(ps);
//	ps = NULL;
//
//	return 0;
//}

struct S
{
	int n;
	char* arr;
};

int main()
{
	struct S* ps = (struct S*)malloc(sizeof(struct S));
	if (ps == NULL)
	{
		return 1;
	}
	ps->n = 100;
	ps->arr = (char*)malloc(sizeof(char) * 10);
	if (ps->arr == NULL)
	{
		return 1;
	}
	//使用
	int i = 0;
	for (i = 0; i < 10; i++)
	{
		ps->arr[i] = 'Q';
	}
	//增加
	char* ptr = (char*)realloc(ps->arr, 20 * sizeof(char));
	if (ptr != NULL)
	{
		ps->arr = ptr;
	}
	else
		return 1;
	for (i = 0; i < 10; i++)
	{
		printf("%c\n", ps->arr[i]);
	}
	free(ps->arr);
	ps->arr = NULL;
	free(ps);
	ps = NULL;

	return 0;
}