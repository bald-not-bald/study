#define _CRT_SECURE_NO_WARNINGS

#include<stdio.h>
#include<string.h>

//int main()
//{
//	char* a[] = { "work","at","alibaba" };
//
//	char** pa = a;
//	pa++;
//	printf("%s\n", *pa);
//
//	return 0;
//}


//**********重点
//int main()
//{
//	char* c[] = { "ENTER","NEW","POINT","FIRST" };
//	char** cp[] = { c + 3,c + 2,c + 1,c };
//	char*** cpp = cp;
//
//	printf("%s\n", **++cpp);
//	printf("%s\n", *-- * ++cpp + 3);
//	printf("%s\n", *cpp[-2] + 3);
//	printf("%s\n", cpp[-1][-1] + 1);
//
//	return 0;
//}



//字符串函数介绍
//strlen
#include<assert.h>
//size_t my_strlen(const char* str)
//{
//	assert(str != NULL);
//	int count = 0;
//	while (*str != '\0')
//	{
//		count++;
//		str++;
//	}
//	return count;
//}
//递归
//size_t my_strlen(const char* str)
//{
//	assert(str != NULL);
//	int count = 0;
//	if (*str != '\0')
//		return 1 + my_strlen(str + 1);
//	else
//		return 0;
//}
//指针 - 指针
//size_t my_strlen(const char* str)
//{
//	const char* start = str;
//	assert(str != NULL);
//	int count = 0;
//	while (*str)
//	{
//		str++;
//	}
//	return str - start;
//}

//strlen是求字符串长度的，求出的长度是不可能为负数的
//所以返回类型为size_t是合情合理的
//typedef unsigned int size_t
//
//size_t strlen(const char* string);

//int main()
//{
//	char arr[] = "abcdef";
//	int len = my_strlen(arr);
//	printf("%d\n", len);
//	return 0;
//}

//int main()
//{
//	if (strlen("abc") - strlen("abcdef") > 0)
//	{
//		printf(">\n");
//	}
//	else
//	{
//		printf("<=\n");
//	}
//	return 0;
//}

//strcpy函数
//char*strcpy(char* destination,const char* source)
//目标空间足够大
//目标空间可修改

//模拟实现
//char* my_strcpy(char* dest, const char* src)
//{
//	char* ret = dest;
//	assert(dest && src);
//	while (*dest++ = *src++)
//	{
//		;
//	}
//	return ret;
//}
//
//int main()
//{
//	char arr1[20] = "xxxxxxxxxxxxx";
//	char arr2[] = "hello bit";
//	my_strcpy(arr1, arr2);
//	printf("%s\n", arr1);
//	printf("%s\n", arr2);
//	return 0;
//}

//strcat函数
//字符串追加

//char* my_strcat(char* dest, const char* src)
//{
//	char* ret = dest;
//	assert(dest && src);
//	//1.寻找目标空间的结尾处的'\0'
//	while (*dest)
//	{
//		dest++;
//	}
//	//2.追加
//	while (*dest++ = *src++)
//	{
//		;
//	}
//	return ret;
//}
//
//int main()
//{
//	char arr1[20] = "hello ";
//	char arr2[] = "world";
//	my_strcat(arr1,arr2);
//	printf("%s\n", arr1);
//	return 0;
//}


//strcmp
//比较大小
//int my_strcmp(char* str1, const char* str2)
//{
//	assert(str1 && str2);
//	while (*str1 == *str2)
//	{
//		if (*str1 == '\0')
//			return 0;
//		str1++;
//		str2++;
//	}
//	//if (*str1 > *str2)
//	//	return 1;
//	//else
//	//	return -1;
//	return *str1 - *str2;
//}


//int main()
//{
//	char arr1[] = "abcdef";
//	char arr2[] = "abq";
//	int ret = my_strcmp(arr1, arr2);
//
//	//if (ret == 1)//不一定返回1 
//	//if(ret>0)
//	//	printf("arr1>arr2\n");
//
//	printf("%d\n", ret);
//	return 0;
//}


//上面的三个字符串函数长度不受限制


//strncpy
//char* strncpy(char*destination,const char*source,size_t n)

//char* my_strncpy(char* dest, const char* src, size_t n)
//{
//	char* ret = dest;
//	assert(dest && src);
//	int i = 0;
//	for (i = 0; i < n; i++)
//	{
//		*dest++ = *src++;
//	}
//	return ret;
//}
//
//int main()
//{
//	char arr1[20] = { 0 };
//	my_strncpy(arr1, "abcdef", 3);
//	printf("%s\n", arr1);
//	return 0;
//}

//strncat

//int main()
//{
//	char arr1[20] = "abcxxxxxxxxxxxx";
//	strncat(arr1, "defqwer", 4);
//	printf("%s\n", arr1);
//	return 0;
//}


//int main()
//{
//	char* p1 = "abcdef";
//	char* p2 = "abcqwer";
//	int ret = strncmp(p1,p2,6);
//	printf("%d", ret);
//	return 0;
//}

//字符串查找函数
//
//暴力匹配
//char* my_strstr(const char* str1, const char* str2)
//{
//	assert(str1 && str2);
//	if (*str2 == '\0')
//	{
//		return (char*)str1;
//	}
//	const char* s1 = str1;
//	const char* s2 = str2;
//	const char* cp = str1;
//	while (*cp)
//	{
//		s1 = cp;
//		s2 = str2;
//		while (*s1 != '\0' && *s2 != '\0' && *s1 == *s2)
//		{
//			s1++;
//			s2++;
//		}
//		if (*s2 == '\0')
//		{
//			return (char*)cp;
//		}
//		cp++;
//	}
//	return NULL;
//}
//
//int main()
//{
//	char arr1[] = "abbbbbbbcdef";
//	char arr2[] = "bbc";
//
//	char* ret = my_strstr(arr1, arr2);
//	if (ret == NULL)
//	{
//		printf("找不到\n");
//	}
//	else
//	{
//		printf("%s\n", ret);
//	}
//
//	return 0;
//}

