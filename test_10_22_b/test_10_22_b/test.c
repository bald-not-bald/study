#define _CRT_SECURE_NO_WARNINGS

#include<stdio.h>
#include<string.h>
//int main()
//{
//	int a = 3;
//	int b = 0;
//
//	int c = a || b;
//	printf("%d\n", c);
//
//	return 0;
//}
//1.能被4整除，并且不能被100整除
//2.能被400整除是闰年

//int is_leap_year(int y)
//{
//	if (((y % 4 == 0) && (y % 100 != 0)) || (y % 400 == 0))
//	{
//		return 1;
//	}
//	else
//		return 0;
//}
//int main()
//{
//	int y = 0;
//	scanf("%d", &y);
//
//	int ret = is_leap_year(y);
//	if (ret == 1)
//	{
//		printf("%d是闰年", y);
//	}
//	else
//	{
//		printf("不是闰年");
//	}
//	return 0;
//}

//int main()
//{
//	int i = 0, a = 1, b = 2, c = 3, d = 4;
//	//i = a++ & ++b & d++;
//	i = a++||++b||d++;
//
//	
//	printf("%d %d %d %d", a, b, c, d);//1 3 3 5
//
//	return 0;
//}

//条件操作符（三目操作符）
//表达式1？表达式2：表达式3
//真       真       假

//int main()
//{
//	int a = 3;
//	int b = 0;
//	int max = (a > b ? a : b);
//	if (a > 5)
//		b = 3;
//	else
//		b = -3;
//
//	(a > 5) ? (b = 3) : (b = -3);
//	b = (a > 5 ? 3 : -3);
//
//	printf("%d", max);
//	return 0;
//}

//int main()
//{
//	int arr[10] = { 0 };
//	//arr[7]-->*(arr+7)
//	//arr就是数组首元素的地址
//	// arr+7就是跳过7个元素，指向了第八个元素
//	//*（arr+7）就是第八个元素
//	//
//	arr[7] = 9;
//	7[arr] = 8;
//
//
//	return 0;
//}
//int Add(int x, int y)
//{
//	return x + y;
//}
//
//int main()
//{
//	int a = 10;
//	int b = 20;
//	int c = Add(a, b);//（）就是函数调用操作符
//
//
//	printf("%d", c);
//	//sizeod a;
//	return 0;
//}

struct Stu
{
	char name[20];
	int age;
	double score;
};
void set_stu(struct Stu* ps)
{
	//strcpy((*ps).name, "zhangsan");
	//(*ps).age = 20;
	//(*ps).score = 100.0;
	strcpy(ps->name, "zhangsan");
	ps->age = 20;
	ps->score = 100.0;
}
void print_stu(struct Stu *ps)
{
	printf("%s %d %lf", ps->name, ps->age, ps->score);
}

int main()
{
	struct Stu s = { 0 };
	set_stu(&s);
	print_stu(&s);

	return 0;
}


