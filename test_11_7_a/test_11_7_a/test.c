#define _CRT_SECURE_NO_WARNINGS

#include<stdio.h>
#include<string.h>
#include<stdlib.h>

//#define MAX 100
//typedef enum Day
//{
//	Mon=1,
//	Tues=2,
//	Wed=3,
//	Thur=4,
//	Fri=5,
//	Sat=6,
//	Sun=7
//}Day;
//
////struct Stu
////{
////	char name[20];
////};
//
//int main()
//{
//	int m = MAX;
//	Day d = 3;
//	printf("%d\n", sizeof(d));
//	printf("%d\n", d);
//	
//	//printf("hehe\n");
//
//	return 0;
//}


//联合体
//联合的成员共用一块内存空间，这样一个联合变量的大小，至少是最大成员的大小
//存在对齐

//union UN
//{
//	char c;
//	int i;
//};
//
//int main()
//{
//	union UN un;
//	printf("%zd\n", sizeof(un));//4
//	printf("%p\n", &un);
//	printf("%p\n", &(un.c));
//	printf("%p\n", &(un.i));
//	return 0;
//}

//union UN
//{
//	char c[5];
//	int n;
//};
//
//int main()
//{
//	printf("%d\n", sizeof(union UN));
//	return 0;
//}

//判断当前计算机的大小端存储
//int check_sys()
//{
//	union UN
//	{
//		char c;
//		int i;
//	}un;
//	un.i = 1;
//	return un.c;
//}
//
//
//
//int main()
//{
//	int a = 1;//0x 00 00 00 01
//	//低 - 高
//	//01 00 00 00 - 小端
//	//00 00 00 01 - 大端
//	//if (*(char*)&a == 1)
//	//{
//	//	printf("小端\n");
//	//}
//	//else
//	//	printf("大端\n");
//	int ret = check_sys();
//
//	if (ret == 1)
//		printf("小端\n");
//	else
//		printf("大端\n");
//
//	return 0;
//}

//动态内存管理
//malloc
//free
//calloc
//realloc
//


//malloc
//在堆区申请的空间

//int main()
//{
//	//申请40个字节的空间,用来存放10个整形
//	int* p = (int*)malloc(40);
//	if (p == NULL)
//	{
//		printf("%s\n", strerror(errno));
//		return 1;
//	}
//	//存放1-10
//	int i = 0;
//	for (i = 0; i < 10; i++)
//	{
//		*(p + i) = i + 1;
//	}
//	//打印
//	for (i = 0; i < 10; i++)
//	{
//		printf("%d ", *(p + i));
//	}
//	//释放内存
//	free(p);
//	//防止非法访问
//	p = NULL;
//	return 0;
//}


//calloc
//
//int main()
//{
//	int*p = (int*)calloc(10, sizeof(int));
//	if (NULL == p)
//	{
//		printf("%s\n", strerror(errno));
//		return 1;
//	}
//	//使用
//	int i = 0;
//	for (i = 0; i < 10; i++)
//	{
//		printf("%d ", *(p + i));
//	}
//
//	//释放
//	free(p);
//	p = NULL;
//
//	return 0;
//}

//realloc

//int main()
//{
//	int*p = (int*)malloc(5 * sizeof(int));
//	if (NULL == p)
//	{
//		printf("%s\n", strerror(errno));
//		return 1;
//	}
//	int i = 0;
//	for (i = 0; i < 5; i++)
//	{
//		*(p + i) = 1;
//	}
//	//不够了，增加5个整形的空间
//	int* ptr = (int*)realloc(p, 10 * sizeof(int));
//	//realloc
//	//
//	if (ptr != NULL)
//	{
//		p = ptr;
//		ptr = NULL;
//	}
//	//继续使用空间
//	//for (i = 0; i < 10; i++)
//	//{
//	//	*(p + i) = 1;
//	//}
//	for (i = 0; i < 10; i++)
//	{
//		printf("%d ", *(p + i));
//	}
//	//释放空间
//	free(p);
//	p = NULL;
//	return 0;
//}

