#define _CRT_SECURE_NO_WARNINGS

#include<stdio.h>

//int main()
//{
//	int i = 0;
//	int arr[10] = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
//	//printf("%p\n", &arr[9]);
//	//printf("%p\n", &i);
//	for (i = 0; i <= 12; i++)
//	{
//		printf("hehe\n");
//		arr[i] = 0;
//	}//数组越界了
//
//	return 0;
//}

//写出好的代码（易于调试）
//1.代码运行正常
// 2.bug很少
// 3.效率高
// 4.可读性高
// 5.可维护性高
// 6.注释清晰
// 7.文档齐全
//
//常见的coding技巧
//1.使用assert
//2.尽量使用const
// 3.养成良好的编码风格
// 4.添加必要的注释
// 5.避免编码的陷阱
// 
//

#include<string.h>
//void my_strcpy(char*dest,char*src)
//{
//	while (*src!='\0')
//	{
//		*dest = *src;
//		dest++;
//		src++;
//	}
//	*dest =*src;
//}
//void my_strcpy(char* dest, char* src)
//{
//	//if (src == NULL || dest == NULL)
//	//{
//	//	return;
//	//}
//
//	//断言
//	//assert中可以放一个表达式，表达式的结果如果为假，就报错，如果为真就无事发生
//	//assert其实在release版本中优化了
//	//assert(src != NULL);
//	//assert(dest != NULL);
//	assert(dest && src);
//
//	while (*dest++ = *src++)
//	{
//		;
//	}
//
//}
//
//int main()
//{
//	//strcpy - 字符串拷贝
//	char arr1[20] = "xxxxxxxxxxxxxx";
//	char arr2[] = "hello";
//	my_strcpy(arr1, arr2);
//
//
//	printf("%s\n", arr1);
//
//	return 0;
//}
//库函数strcpy返回的是目标空间的起始地址
//

char* my_strcpy(char* dest, const char* src)
{
	//assert(dest && src);//断言指针的有效性
	char* ret = dest;
	while (*dest++ = *src++)
	{
		;
	}
	return ret;

}
int main()
{
	char arr1[20] = { 0 };
	char* p = "hello";
	printf("%s\n", my_strcpy(arr1,p));
	return 0;
}

//int main()
//{
//	//int num = 10;
//	//int* p = &num;
//	//*p = 20;
//
//	////num = 20;//
//	//printf("%d\n", num);
//
//	const int num = 10;
//	//num = 20;//因为num被const修饰，不能被修改
//	int* p =&num;
//	*p = 20;
//	printf("%d\n", num);
//	return 0;
//}
//const修饰指针变量的时候
//int num = 10;
// int* p = &num;
// int n = 1000;
// 1.const放在*左边，修饰的是指针指向的内容，表示指针指向的内容，不能通过指针来改变了；但是指针变量本身可以修改
// const int* p = &num;
// *p = 20;//err
// p = &n;//ok
// 
// 2.const放在*的右边，const修饰的指针变量本身，表示指针比那里那个本身的内容不能被修改，但是指针指向的内容，可以通过指针来进行修改
// int* const p = &num;
// *p = 20;//ok
// p = &n;//err
// 