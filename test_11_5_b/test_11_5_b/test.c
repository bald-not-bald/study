#define _CRT_SECURE_NO_WARNINGS

#include<stdio.h>
#include<string.h>


//指针面试题
//int main()
//{
//	int a[5] = { 1,2,3,4,5 };
//	int* ptr = (int*)(&a + 1);
//	printf("%d %d", *(a + 1), *(ptr - 1));
//	return 0;
//}

//struct Test
//{
//	int Num;
//	char* pcName;
//	short sDate;
//	char cha[2];
//	short sBa[4];
//}*p;
////假设p的值为0x100000.如下表达式的值分别为多少
//int main()
//{
//	printf("%p\n", p + 0x1);
//
//	printf("%p\n", (unsigned long)p + 0x1);
//
//	printf("%p\n", (unsigned int*)p + 0x1);
//}

//小端,x86环境
//int main()
//{
//	int a[4] = { 1,2,3,4 };
//	int* ptr1 = (int*)(&a + 1);
//	int* ptr2 = (int*)((int)a + 1);
//	printf("%x,%x",ptr1[-1], *ptr2);
//	return 0;
//}

//int main()
//{
//	int a[3][2] = { (0,1),(2,3),(4,5) };
//	int* p;
//
//	p = a[0];
//
//	printf("%d", *p);
//	return 0;
//}

int main()
{
	int a[5][5];

	int(*p)[4];

	p = a;
	//p的类型是int(*)[4]   a的类型是int(*)[5]
	printf("%p,%d\n", &p[4][2] - &a[4][2], &p[4][2] - &a[4][2]);
	//-4
	//10000000000000000000000000000100
	//11111111111111111111111111111011
	//11111111111111111111111111111100
	//0xfffffffc

	return 0;
}