#define _CRT_SECURE_NO_WARNINGS

#include<stdio.h>
#include<string.h>


//strtok分割符
//int main()
//{
//	//char arr[] = "zpengwei@yesh.net";
//	char arr[] = "15654#12345.123.85";
//	char* p = "#.";
//	char buf[20] = { 0 };
//	strcpy(buf, arr);
//	char* ret = NULL;
//	for (ret = strtok(buf, p); ret != NULL; ret = strtok(NULL, p))
//	{
//		printf("%s\n", ret);
//	}
//	//char* ret = strtok(buf, p);
//	//printf("%s\n", ret);
//
//	//ret = strtok(NULL, p);
//	//printf("%s\n", ret);
//
//	//ret = strtok(NULL, p);
//	//printf("%s\n", ret);
//
//
//	return 0;
//}

//strerror
//返回错误码。将错误码翻译成错误信息

//C语言的库函数在运行的时候，如果发生错误，就会将错误码存放到一个变量中，这个变量是:errno

//int main()
//{
//	printf("%s\n", strerror(0));
//	printf("%s\n", strerror(1));
//	printf("%s\n", strerror(2));
//	printf("%s\n", strerror(3));
//	printf("%s\n", strerror(4));
//	printf("%s\n", strerror(5));
//	return 0;
//}

//fopen
//如果打开文件成功，就返回一个有效的指针
//#include<errno.h>
//int main()
//{
//	//打开文件
//	FILE* pf = fopen("test.txt", "r");
//	if (pf == NULL)
//	{
//		printf("%s\n",strerror(errno));
//		return 1;
//	}
//	//读文件
//	//关闭文件
//	fclose(pf);
//	return 0;
//}


//perror
//打印错误信息

//#include<errno.h>
//int main()
//{
//	//打开文件
//	FILE* pf = fopen("test.txt", "r");
//	if (pf == NULL)
//	{
//		//printf("%s\n",strerror(errno));
//		perror("fopen");
//		return 1;
//	}
//	//读文件
//	//关闭文件
//	fclose(pf);
//	return 0;
//}
#include<ctype.h>
//字符分类函数
//int main()
//{
//	int ret = isdigit('Q');
//	printf("%d\n", ret);
//	return 0;
//}

//int main()
//{
//	char arr[] = "I HAVE AN Apple.";
//	int i = 0;
//	while (arr[i])
//	{
//		if (isupper(arr[i]))
//		{
//			printf("%c", tolower(arr[i]));
//		}
//		else
//		{
//			printf("%c", arr[i]);
//		}
//		i++;
//	}
//	return 0;
//}

//内存系列函数
//memcpy
//memmove
//memcmp
//memset
//
#include<assert.h>
//void* my_memcpy(void* dest, const void* src,size_t num)
//{
//	void* ret = dest;
//	assert(dest && src);
//	while (num--)
//	{
//		*(char*)dest = *(char*)src;
//		//dest = (char*)dest + 1;
//		//src = (char*)src + 1;
//		++(char*)dest;
//		++(char*)src;
//	}
//	return ret;
//}

//int main()
//{
//	int arr1[] = { 1,2,3,4,5,6,7,8,9,10 };
//	int arr2[10] = { 0 };
//	//内存拷贝 - memcpy
//	my_memcpy(arr2, arr1, 20);
//	
//	return 0;
//}

//test1()
//{
//	int arr1[] = { 1,2,3,4,5,6,7,8,9,10 };
//	int arr2[10] = { 0 };
//	//内存拷贝 - memcpy
//	my_memcpy(arr2, arr1, 20);
//}
//test2()
//{
//	int arr1[] = { 1,2,3,4,5,6,7,8,9,10 };
//	my_memcpy(arr1+2, arr1, 20);
//
//}
//int main()
//{
//	test2();
//	return 0;
//}


//memmove
//实现重叠内存的拷贝
//void* my_memmove(void* dest, const void* src, size_t num)
//{
//	void* ret = dest;
//	assert(dest && src);
//	if (dest < src)
//	{
//		//前->后
//		while (num--)
//		{
//			*(char*)dest = *(char*)src;
//			dest = (char*)dest + 1;
//			src = (char*)src + 1;
//		}
//	}
//	else
//	{
//		//后->前
//		while (num--)
//		{
//			*((char*)dest+num) = *((char*)src + num);
//		}
//	}
//	return ret;
//}
//test2()
//{
//	int arr1[] = { 1,2,3,4,5,6,7,8,9,10 };
//	my_memmove(arr1 + 2, arr1, 20);
//	int i = 0;
//	for (i = 0; i < 10; i++)
//	{
//		printf("%d\n", arr1[i]);
//	}
//}
//int main()
//{
//	test2();
//
//
//	return 0;
//}

//memcmp
//int main()
//{
//	int arr1[] = { 1,2,3 };
//	int arr2[] = { 1,2,5 };
//	int ret = memcmp(arr1, arr2, 9);
//	printf("%d\n", ret);
//	return 0;
//}

//memset - 内存设置函数
//以字节为单位来设置内存中的数据
//
//int main()
//{
//	//char arr[] = "Hello World";
//	//memset(arr, 'x', 5);
//	//printf("%s\n", arr);
//	//memset(arr + 6, 'y', 5);
//	//printf("%s\n", arr);
//	int arr[10] = { 0 };
//	memset(arr, 0, 40);
//
//	return 0;
//}


