#define _CRT_SECURE_NO_WARNINGS

#include<stdio.h>
//递归与迭代
//int fac(int n)
//{
//	//if (n <= 1)
//	//	return 1;
//	//else
//	//	return n * fac(n - 1);
//	int i = 0;
//	int ret = 1;
//	for (i = 1; i <=n; i++)
//	{
//		ret *= i;
//	}
//	return ret;
//}
//
//
//int main()
//{
//	int n = 0;
//	scanf("%d", &n);
//
//	int ret = fac(n);
//
//	printf("ret = %d\n", ret);
//
//	return 0;
//}


//求第n个斐波那契数列
//int Fib(int n)
//{
//	if (n <= 2)
//		return 1;
//	else
//		return Fib(n - 1) + Fib(n - 2);
//}

//迭代解决
//int Fib(int n)
//{
//	int a = 1;
//	int b = 1;
//	int c = 1;
//
//	while (n >= 3)
//	{
//		c = a + b;
//		a = b;
//		b = c;
//		n--;
//	}
//	return c;
//}

//int main()
//{
//	int a = 0;
//	scanf("%d", &a);
//	int ret = Fib(a);
//	printf("%d\n", ret);
//	return 0;
//}


//void test(int n)
//{
//	if (n < 10000)
//		test(n + 1);
//}
//int main()
//{
//	test(1);
//	return 0;
//}

//递归经典项目
//汉诺塔问题
//void hanoi(int n, char rod1, char rod2, char rod3)
//{
//	if (1 == n)
//		//如果只有一个盘子就从起始柱子挪到目标柱子
//	{
//		printf("%c to %c\n", rod1, rod2);
//		return;
//	}
//	//将n-1个盘子借助目标柱子移动到辅助柱子
//	hanoi(n - 1, rod1, rod3, rod2);
//	//将第n个盘子从起始柱子移动到目标柱子
//	printf("%c to %c\n", rod1, rod2);
//	//将n-1个盘子从辅助柱子借助起始柱子移动到目标柱子
//	hanoi(n - 1, rod3, rod2, rod1);
//}
//
//int main()
//{
//	//输入汉诺塔盘子数量
//	int a = 0;
//	scanf("%d", &a);
//	//生成函数
//	hanoi(a, 'A', 'C', 'B');
//	return 0;
//}





//青蛙跳台阶问题

//int fgj(int n, int m)
//{
//	if (0 == n)
//		return 0;
//	else if (1 == n)
//		return 1;
//	else
//		//跳到第n个台阶只能从n-1或者n-2的台阶跳上来
//		return fgj(n - 1, m) + fgj(n - 2, m);
//}
//
//int main()
//{
//	int n = 0;
//	int m = 2;
//	scanf("%d", &n);
//
//	int way = fgj(n, m);
//	printf("青蛙跳%d台阶有%d种跳法", n, way);
//
//	return 0;
//}





