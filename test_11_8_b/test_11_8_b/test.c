#define  _CRT_SECURE_NO_WARNINGS 1

#include<stdio.h>
#include<string.h>
#include<stdlib.h>


//#pragma pack(4)
//struct A
//{
//	short a;
//	char b;
//	long c;
//	long d;
//};
//struct B
//{
//	int a;//4
//	short b;//2
//	char c;//1
//	int d;//4
//};
//#pragma pack()
//
//int main()
//{
//	printf("%zd\n", sizeof(struct A));
//	printf("%zd\n", sizeof(struct B));
//	return 0;
//}


//typedef struct
//{
//	int a;
//	char b;
//	short c;
//	short d;
//}AA;
//
//int main()
//{
//	printf("%zd\n", sizeof(AA));
//
//	return 0;
//}
//#define A 2
//#define B 3
//
//#define MAX_SIZE A+B
//struct _Record_Struct
//{
//	unsigned char Env_Alarm_ID : 4;
//	unsigned char Paral : 2;
//	unsigned char state;
//	unsigned char avail : 1;
//}*Env_Alarm_Record;
//
//int main()
//{
//	//struct _Record_Struct* pointer = (struct _Record_Struct*)malloc(sizeof(struct _Record_Struct) * 2 + 3);
//	printf("%d\n", sizeof(struct _Record_Struct)*MAX_SIZE);
//	return 0;
//}


//编写一个函数找出一个数组中两个只出现一次的数字
//分组
//分组的要领，就是将两个单身狗必须放在两个组，同时每个组剩余的数字都是成对出现的

void find_single_dog(int arr[], int sz, int single_dog[])
{
	int ret = 0;
	int i = 0;
	for (i = 0; i < sz; i++)
	{
		ret ^= arr[i];
	}
	//计算ret的那一位二进制位是1
	int pos = 0;
	for (i = 0; i < 32; i++)
	{
		if (((ret >> 1) & 1) == 1)
		{
			pos = i;
			break;
		}
	}
	for (i = 0; i < sz; i++)
	{
		if (((arr[i] >> pos) & 1) == 1)
		{
			single_dog[0] ^= arr[i];
		}
		else
		{
			single_dog[1] ^= arr[i];
		}
	}
}

int main()
{
	int arr[] = { 1,2,3,4,5,1,2,3,4,6 };
	int sz = 0;
	sz = sizeof(arr) / sizeof(arr[0]);
	int single_dog[2] = { 0 };


	find_single_dog(arr, sz,single_dog);
	printf("%d %d\n", single_dog[0], single_dog[1]);
	return 0;
}

