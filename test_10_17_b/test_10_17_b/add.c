#define _CRT_SECURE_NO_WARNINGS


//全局变量

//int g_val = 2022;

//static修饰全局变量的时候，这个全局变量的外部链接属性就变成了内部链接属性。其他源文件（.c）就不能再使用到这个全局变量了。

//int Add(int x, int y)
//{
//	return x + y;
//}