#define _CRT_SECURE_NO_WARNINGS

#include<stdio.h>
//数组参数
//一维数组传参
//void test(int arr[])//
//{}
//void test(int arr[10])//
//{}
//void test(int *arr)//
//{}
//void test2(int *arr[20])//
//{}
//void test2(int **arr)//
//{}
//
//int main()
//{
//	int arr[10] = { 0 };
//	int* arr2[20] = { 0 };
//
//	test(arr);
//	tesr2(arr2);
//
//	return 0;
//}

//二维数组传参
//void test(int arr[3][5])//yes
//{}
////void test(int arr[][])//no
////{}
//void test(int arr[][5])//yes
//{}
//
//
//int main()
//{
//	int arr[3][5] = { 0 };
//	test(arr);
//	return 0;
//}
// 

//一级指针传参
//void print(int* p, int sz)
//{
//	int i = 0;
//	for (i = 0; i < sz; i++)
//	{
//		printf("%d\n", *(p + i));
//	}
//}
//int main()
//{
//	int arr[10] = { 1,2,3,4,5,6,7,8,9 };
//	int* p = arr;
//	int sz = sizeof(arr) / sizeof(arr[0]);
//
//	print(p, sz);
//	return 0;
//}


//二级指针传参
//void test(int** ptr)
//{
//	printf("num = %d\n", **ptr);
//}
//int main()
//{
//	int n = 10;
//	int *p = &n;
//	int **pp = &p;
//	test(pp);
//	test(&p);
//	return 0;
//}
