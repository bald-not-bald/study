#define _CRT_SECURE_NO_WARNINGS

#include<stdio.h>
//int main()
//{
//	int arr1[5] = { 1,2,3,4,5 };
//	int arr2[5] = { 2,3,4,5,6 };
//	int arr3[5] = { 3,4,5,6,7 };
//	int arr4[5] = { 0,0,0,0,0 };
//	//指针数组
//	int* arr[4] = { arr1,arr2,arr3,arr4 };
//	int i = 0;
//	for (i = 0; i < 4; i++)
//	{
//		int j = 0;
//		for(j = 0;j<5;j++)
//		{
//			printf("%d ", *(arr[i] + j));
//		}
//		printf("\n");
//	}
//
//	return 0;
//}


//数组指针
// 数组指针的定义
// 存放数组首元素地址的指针
// 
//
//
//int main()
//{
//	char ch = 'w';
//	char* pc = &ch;
//
//	int num = 10;
//	int* pi = &num;
//
//	int arr[10] = { 0 };
//	//pa就是一个数组指针
//	int(*pa)[10] = &arr;
//
//
//	return 0;
//}


//int main()
//{
//	int arr[10] = { 1,2,3,4,5,6,7,8,9,10 };
//	int(*p)[10] = &arr;
//
//	//printf("%p\n", arr);//int*
//	//printf("%p\n", arr + 1);//+4
//
//	//printf("%p\n", &arr[0]);//int*
//	//printf("%p\n", &arr[0] + 1);//+4
//
//	//printf("%p\n", &arr);//int(*p)[10]
//	//printf("%p\n", &arr + 1);//跳过一个数组
//
//	char arr[5];
//	char (*pc)[5] = &arr;
//
//
//
//
//	return 0;
//
//}


//数组指针的用法

//int main()
//{
//	int arr[10] = { 1,2,3,4,5,6,7,8,9,10 };
//	int(*p)[10] = &arr;
//	int i = 0;
//	for (i = 0; i < 10; i++)
//	{
//		printf("%d ", (*p)[i]);
//	}
//
//	//int* p = arr;
//	//int i = 0;
//	//for (i = 0; i < 10; i++)
//	//{
//	//	printf("%d ", * (p + i));
//	//}
//
//	return 0;
//
//}
//print1(int arr[3][4], int r, int c)
//{
//	int i = 0;
//	for (i = 0; i < 3; i++)
//	{
//		int j = 0;
//		for (j = 0; j < 4; j++)
//		{
//			printf("%d ", arr[i][j]);
//		}
//		printf("\n");
//	}
//}
void print2(int(*p)[4],int r,int c)
{
	int i = 0;
	for (i = 0; i < r; i++)
	{
		int j = 0;
		for (j = 0; j < c; j++)
		{
			//printf("%d ", (*(p + i))[j]);
			printf("%d ", p[i][j]);
		}
		printf("\n");
	}
}

int main()
{
	int arr[3][4] = { 1,2,3,4,2,3,4,5,3,4,5,6, };
	//print1(arr,3,4);
	print2(arr, 3, 4);
	return 0;
}