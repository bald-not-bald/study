#define _CRT_SECURE_NO_WARNINGS

#include<stdio.h>


//循环语句



//int main()
//{
//	while (1)
//	{
//		printf("hehe\n");
//	}
//
//
//	return 0;
//}



//int main()
//{
//	int i = 1;
//	while (i <= 10)
//	{
//		if (5 == i)
//			break;
//		//break 结束这个循环
//
//		printf("%d ", i);
//		i++;
//	}
//
//	return 0;
//}

//while循环中的
//break是用于永远的终止循环
//continue的作用是跳过本次循环continue后面的代码，直接判断能否执行下一次循环

//int main()
//{
//	int i = 1;
//	while (i <= 10)
//	{
//		i++;
//		if (5 == i)
//			continue;
//		//continue的作用是跳过本次循环continue后面的代码，直接判断能否执行下一次循环
//
//		printf("%d ", i);
//	}
//	return 0;
//}

//int main()
//{
//	//int ch = getchar();
//
//
//	//printf("%c\n", ch);
//	//putchar(ch);
//	/*int ch = 0;*/
//	//while ((ch = getchar()) != EOF)
//	//{
//	//	putchar(ch);
//	//}
//
//	//举一个例子
//	//加入密码是一个字符串
//	char password[20] = { 0 };
//	printf("请输入密码:>");
//	scanf("%s", password);
//	//
//
//	//getchar();
//	int ch = 0;
//	while ((ch = getchar()) != '\n')
//	{
//		;
//	}
//	printf("请确认密码(Y/N):>");
//	int ret = getchar();
//	if ('Y' == ret)
//	{
//		printf("Yes\n");
//	}
//	else
//	{
//		printf("No\n");
//	}
//	
//	return 0;
//}


//int main()
//{
//	char ch = '\0';
//	while ((ch = getchar()) != EOF)
//	{
//		if (ch < '0' || ch>'9')
//			continue;
//		putchar(ch);
//	}
//	//只打印数字字符
//	return 0;
//}



