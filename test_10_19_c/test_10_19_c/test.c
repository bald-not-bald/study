#define _CRT_SECURE_NO_WARNINGS


#include<stdio.h>

//函数的声明和定义

//#include"add.h"
//#include"sub.h"

//导入静态库
//#pragma comment(lib,"add.lib")
//
//int main()
//{
//	int a = 0;
//	int b = 0;
//	scanf("%d %d", &a, &b);
//	//加法
//	int sum = Add(a, b);
//	printf("%d\n", sum);
//	//减法
//	//int sub = Sub(a, b);
//	//printf("%d\n", sub);
//
//	return 0;
//}

//函数递归
//接受一个整型值（无符号），按照顺序打印它的每一位

//%d是打印有符号的整数（会有正负数）
//%u是打印无符号的整数
// 
// 递归的实现
//递归存在两个必要条件
//存在限制条件，满足这个限制条件的时候，递归便不再继续
//每次递归调用之后越来越接近这个限制条件


//void print(unsigned int n)
//{
//	if (n > 9)
//	{
//		print(n / 10);
//	}
//	printf("%d ", n % 10);
//}
//
//int main()
//{
//	unsigned int num = 0;
//	scanf("%u", &num);
//	print(num);//接受一个整形值（无符号），按照顺序打印他的每一位
//
//	return 0;
//}






//int main()
//{
//	unsigned int num = 0;
//	scanf("%d",&num);//1234
//
//	//1 2 3 4
//	while (num)
//	{
//		printf("%d ", num % 10);
//		num = num / 10;
//	}
//
//
//	return 0;
//}


//练习
//编写函数不允许创建临时变量，求字符串的长度


//求字符串的长度
//模拟实现strlen


#include<string.h>
//int my_strlen(char* str)//参数部分写出指针的形式
//{
//	int count = 0;//计数，临时变量
//	while (*str != '\0')
//	{
//		count++;
//		str++;//找下一个字符
//	}
//	return count;
//}



//递归求解

int my_strlen(char* str)
{
	if (*str != '\0')
	{
		return 1 + my_strlen(str + 1);
	}
	else
		return 0;
}


int main()
{
	char arr[] = "abc";
	int len = my_strlen(arr);
	printf("%d\n", len);
	return 0;
}






