#define _CRT_SECURE_NO_WARNINGS

#include<stdio.h>

//int main()
//{
//	//该代码是一次函数调用
//	//调用0地址处的一个函数
//	//首先代码中将0强制类型转换为类型位void（*）（）的函数指针
//	//然后去调用0地址处的函数
//	(*(void(*)())0)();
//	//该代码是一次函数声明
//	//声明的函数叫signal
//	//函数的参数有两个，一个是int类型，一个是函数指针类型
//	//该函数指针能够指向的那个函数的参数是int
//	//返回类型是void
//	//signal函数的返回类型是一个函数指针，该函数指针能够指向的那个函数的参数是int
//	//返回类型是void
//	typedef void(*pf_t)(int);
//	pf_t signal(int pf_t);
//	void (*signal(int, void(*)(int)))(int);
//
//	return 0;
//}

//函数指针数组

//int main()
//{
//	int(*pfA[5])(const char*);
//	return 0;
//}

//写一个计算器整数加减乘除
//a&b,a|b 
//void menu()
//{
//	printf("***********************************\n");
//	printf("******** 1. add    2. sub   *******\n");
//	printf("******** 3. mul    4. div   *******\n");
//	printf("******** 0. exit            *******\n");
//	printf("***********************************\n");
//}
//int Add(int x, int y)
//{
//	return x + y;
//}
//int Sub(int x, int y)
//{
//	return x - y;
//}
//int Mul(int x, int y)
//{
//	return x * y;
//}
//int Div(int x, int y)
//{
//	return x / y;
//}
//
//
//
//int main()
//{
//	int input = 0;
//	int x = 0;
//	int y = 0;
//	int ret = 0;
//	do 
//	{
//		menu();
//		printf("请选择:");
//		scanf("%d", &input);
//		switch (input)
//		{
//		case 1:
//			printf("请输入两个操作数\n");
//			scanf("%d %d", &x, &y);
//			ret = Add(x, y);
//			printf("%d\n", ret);
//			break;
//		case 2:
//			printf("请输入两个操作数\n");
//			scanf("%d %d", &x, &y);
//			ret = Sub(x, y);
//			printf("%d\n", ret);
//			break;
//		case 3:
//			printf("请输入两个操作数\n");
//			scanf("%d %d", &x, &y);
//			ret = Mul(x, y);
//			printf("%d\n", ret);
//			break;
//		case 4:
//			printf("请输入两个操作数\n");
//			scanf("%d %d", &x, &y);
//			ret = Div(x, y);
//			printf("%d\n", ret);
//			break;
//		case 0:
//			printf("退出计算器\n");
//			break;
//		default:
//			printf("请选择\n");
//		}
//	} while (1);
//
//	return 0;
//}



//void menu()
//{
//	printf("***********************************\n");
//	printf("******** 1. add    2. sub   *******\n");
//	printf("******** 3. mul    4. div   *******\n");
//	printf("******** 0. exit            *******\n");
//	printf("***********************************\n");
//}
//int Add(int x, int y)
//{
//	return x + y;
//}
//int Sub(int x, int y)
//{
//	return x - y;
//}
//int Mul(int x, int y)
//{
//	return x * y;
//}
//int Div(int x, int y)
//{
//	return x / y;
//}


//函数指针数组存放上述函数的地址
//转移表
//int (*pf[5])(int, int) = { NULL,Add,Sub,Mul,Div };
//int main()
//{
//	int input = 0;
//	int x = 0;
//	int y = 0;
//	int ret = 0;
//	do
//	{
//		menu();
//		printf("请选择:");
//		scanf("%d", &input);
//		if (input == 0)
//		{
//			printf("退出计算器");
//		}
//		else if (input >= 1 && input <= 4)
//		{
//			printf("请输入两个操作数\n");
//			scanf("%d %d", &x, &y);
//			ret = pf[input](x, y);
//			printf("%d\n", ret);
//		}
//		else
//		{
//			printf("选择错误\n");
//		}
//
//		
//	} while (input);
//
//	return 0;
//}


//int main()
//{
//	int arr[10];
//	int (*pa)[10] = &arr;
//	//函数指针数组
//	int (*pf[5])(int, int);
//	//ppf是指向函数指针数组的指针
//	int (*(*ppf)[5])(int, int) = &pf;
//
//	return 0;
//}


//void menu()
//{
//	printf("***********************************\n");
//	printf("******** 1. add    2. sub   *******\n");
//	printf("******** 3. mul    4. div   *******\n");
//	printf("******** 0. exit            *******\n");
//	printf("***********************************\n");
//}
//int Add(int x, int y)
//{
//	return x + y;
//}
//int Sub(int x, int y)
//{
//	return x - y;
//}
//int Mul(int x, int y)
//{
//	return x * y;
//}
//int Div(int x, int y)
//{
//	return x / y;
//}
//
//
//void calc(int (*pf)(int, int))
//{
//	int x = 0;
//	int y = 0;
//	int ret = 0;
//	printf("请输入两个操作数\n");
//	scanf("%d %d", &x, &y);
//	ret = pf(x, y);
//	printf("%d\n", ret);
//}
//int main()
//{
//	int input = 0;
//
//	do
//	{
//		menu();
//		printf("请选择:");
//		scanf("%d", &input);
//		switch (input)
//		{
//		case 1:
//			calc(Add);
//			break;
//		case 2:
//			calc(Sub);
//			break;
//		case 3:
//			calc(Mul);
//			break;
//		case 4:
//			calc(Div);
//			break;
//		case 0:
//			printf("退出计算器\n");
//			break; 
//		default :
//			printf("请选择\n");
//			break;
//		}
//		
//	} while (input);
//
//	return 0;
//}

//void bubble_sort(int arr[], int sz)
//{
//	int i = 0;
//	for (i = 0; i < sz - 1; i++)
//	{
//		//一趟冒泡排序的过程
//		int j = 0;
//		for (j = 0; j < sz-1-i; j++)
//		{
//			if (arr[j] > arr[j + 1])
//			{
//				int tmp = arr[j];
//				arr[j] = arr[j + 1];
//				arr[j + 1] = tmp;
//			}
//		}
//	}
//}



int cmp_int(const void* e1, const void* e2)
{

}
int main()
{
	//对数组进行排序
	int arr[] = { 9,8,7,6,5,4,3,2,1,0 };
	int sz = sizeof(arr) / sizeof(arr[0]);
	//bubble_sort(arr, sz);
	//库函数中有一个排序函数
	qsort(arr, sz, sizeof(arr[0]), cmp_int);
	int i = 0;
	for (i = 0; i < sz; i++)
	{
		printf("%d ", arr[i]);
	}
	return 0;
}


//void qsort(void* base,
//	size_t num,
//	size_t width,
//	int (*cmp)(const void* e1, const void* e2)
//);