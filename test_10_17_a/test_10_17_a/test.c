#define _CRT_SECURE_NO_WARNINGS

//第一二次作业

#include<stdio.h>
#include<string.h>


//输入一些简单的内容
//小飞机
//#include<stdio.h>
//
//int main()
//{
//	printf("     **\n");
//	printf("     **\n");
//	printf("***********\n");
//	printf("***********\n");
//	printf("    *  *\n");
//	printf("    *  *\n");
//	return 0;
//}


//int main()
//{
//	printf("Name    Age    Gender\n");
//	printf("-----------------------\n");
//	printf("Jack    18     man\n");
//	return 0;
//}


//int main()
//{
//	printf("I lost my cellphone\n");
//	return 0;
//}



//int main()
//{
//	int a = 40;
//	int c = 212;
//	int sum = (-8 + 22) * a - 10 + c /2;
//	printf("%d\n",sum);
//	return 0;
//}



//  0   - 数字0
// '0'  - 字符零 - ASCII值是48
//'\0'  - 字符   - ASCII值是0
//EOF - end of file 是文件结束标志 - 值是-1


//int main()
//{
//	int arr[10];//10个元素
//	int arr2[] = { 1,2,3 };//arr2数组有3个元素
//	return 0;
//}

//int main()
//{
//	printf("%d\n", strlen("c:\test\121"));
//
//	return 0;
//}

//int main()
//{
//	char arr[] = { 'b','i','t' };
//	//不完全初始化，剩余的部分默认初始化为0
//	printf("%d\n", strlen(arr));//没有\0作为终止，长度随机值
//	return 0;
//}


//c99标准之前，数组的大小都是用常量或者常量表达式来制定
//c99标准之后，支持了变长数组，这个时候允许数组的大小是变量，但是这种指定方式的数组是不能初始化的
//VS对c99中的一些语法支持不是很好，不支持变长数组


//求两个数的较大值
//int MAX(int x, int y)
//{
//	if (x > y)
//		return x;
//	else
//		return y;
//}
//
//
//int main()
//{
//	int a = 0;
//	int b = 0;
//	scanf("%d%d", &a, &b);
// 
//	int r = MAX(a, b);
//	printf("%d\n", r);
//
//	return 0;
//}



//int main()
//{
//	//输入
//	int x = 0;
//	int y = 0;
//		scanf("%d", &x);
//	//计算过程
//	if (x > 0)
//		y = -1;
//	else if (x == 0)
//		y = 0;
//	else
//		y = 1;
//	//输出
//	printf("%d\n", y);
//	return 0;
//}


//int main()
//{
//	//int a = 7 / 2;
//	int b = 7 % 2;//取模
//	//除号的两端都是整数的时候，执行的是整数除法，如果两端只要有一个浮点数就进行浮点数的除法
//	float a = 7 / 2.0;
//
//	printf("%.2f\n", a);
//	printf("%d\n", b);
//
//	return 0;
//}


//int main()
//{
//	int a = 0;//初始化
//	a = 20;//赋值
//
//	a = a + 3;
//	a += 3;//和上面等效
//
//
//
//	return 0;
//
//}


//C语言中
//0表示假
// 非0表示真
//
//int main()
//{
//	//int flag = 0;
//	//if (!flag)
//	//{
//	//	printf("hehe\n");
//	//}
//
//	/*int a = -10;*/
//
//	//sizeof是操作符，是单目操作符
//	//int a = 10;
//	//printf("%d\n", sizeof(a));
//	//printf("%d\n", sizeof(int));
//	//printf("%d\n", sizeof(a));
//
//
//	//int arr[10] = { 0 };
//	//printf("%d\n", sizeof(arr));//40计算的是整个数组的大小，单位是字节
//	//printf("%d\n", sizeof(arr[0]));
//	//printf("%d\n", sizeof(arr)/sizeof(0));
//
//	//int a = 10;
//	////int b = a++;//后置++，先使用，后++
//	//int b = ++a;//前置++，前加后使用
//	//printf("%d\n", a);
//	//printf("%d\n", b);
//
//	//int a = (int)3.14;
//	//printf("%d\n", a);
//
//	//3.14字面浮点数，编译器默认理解为double类型
//
//
//	return 0;
//}


//int main()
//{
//	int a = 10;
//	if (a == 3)//==是判断相等
//	{
//		printf("hehe\n");
//	}
//	return 0;
//}

//int main()
//{
//	//&&逻辑与 - 并且
//	//||逻辑或 - 或者
//
//	//int a = 0;
//	//int b = 20;
//	//if (a && b)
//	//{
//	//	printf("hehe\n");
//	//}
//	int a = 0;
//	int b = 0;
//	if (a || b)
//	{
//		printf("hehe\n");
//	}
//
//	return 0;
//}

//三目操作符
//int main()
//{
//	int a = 10;
//	int b = 20;
//	int r = (a > b ? a : b);
//	printf("%d\n", r);
//	return 0;
//}



//逗号表达式就是逗号隔开的一串表达式
//逗号表达式的特点是:从左往右依次计算，整个表达式的结果是最后一个表达式的结果
//int main()
//{
//	int a = 10;
//	int b = 20;
//	int c = 0;
//	//
//	int d = (c = a - 2, a = b + c, c - 3);
//	printf("%d\n", d);
//	return 0;
//}

//int main()
//{
//	int arr[10] = { 1,2,3,4,5,6,7,8,9,10 };
//	int n = 3;
//	arr[n] = 20;//[]就是下标引用操作符，arr和3就是[]的操作数
//
//	//a+b
//	return 0;
//}



//函数调用操作符（）
//int Add(int x, int y)
//{
//	return x + y;
//}
//int main()
//{
//	int sum = Add(2, 3);//()就是函数调用操作符
//	return 0;
//}

//int main()
//{
//	/*auto int a = 10;*///自动变量
//
//	return 0;
//}


