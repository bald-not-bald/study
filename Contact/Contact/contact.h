#pragma once

#include<stdio.h>
#include<string.h>
#include<assert.h>
#include<stdlib.h>

#define MAX 100
#define NAME_MAX 20
#define SAX_MAX 5
#define ADDR_MAX 30
#define TELE_MAX 12
#define DEFAULT_SZ 3
#define INC_SZ 2

//人的信息
typedef struct PeoInfo
{
	char name[NAME_MAX];
	int age;
	char sex[SAX_MAX];
	char addr[ADDR_MAX];
	char tele[TELE_MAX];
}PeoInfo;

//静态版本
//typedef struct Contact
//{
//	PeoInfo data[MAX];//存放信息
//	int sz;//当前已经存放的信息的个数
//}Contact;

//动态增长版本
typedef struct Contact
{
	PeoInfo* data;//data指向存放人的信息的空间
	int sz;//当前已经存放的信息的个数
	int capacity;//当前通讯录的最大容量
}Contact;


//初始化通讯录
void InitContact(Contact* pc);

//销毁通讯录
void DestoryContact(Contact* pc);

//增加联系人
void AddContact(Contact* pc);

//删除联系人
void DelContact(Contact* pc);

//查找联系人
void SearchContact(const Contact* pc);

//修改联系人
void ModifyContact(Contact* pc);

//显示通讯录中的信息
void ShowContact(const Contact* pc);

//排序联系人
void SortContact(Contact* pc);




