#define _CRT_SECURE_NO_WARNINGS

#include"contact.h"
void menu()
{
	printf("************************************\n");
	printf("******   1. add     2. del    ******\n");
	printf("******   3. search  4. modify ******\n");
	printf("******   5. show    6. sort   ******\n");
	printf("******   0. exit              ******\n");
	printf("************************************\n");
}

enum Option
{
	EXIT,
	ADD,
	DEL,
	SEARCH,
	MODIFY,
	SHOW,
	SORT
};

int main()
{
	int input = 0;
	//创建通讯录
	Contact con;
	//初始化通讯录
	InitContact(&con);

	do
	{
		menu();
		printf("请选择:>");
		scanf("%d", &input);
		switch (input)
		{
		case ADD:
			//增加个人信息
			AddContact(&con);
			break;
		case DEL:
			//删除个人信息
			DelContact(&con);
			break;
		case SEARCH:
			//查找个人信息
			SearchContact(&con);
			break;
		case MODIFY:
			//修改个人信息
			ModifyContact(&con);
			break;
		case SHOW:
			//显示个人信息
			ShowContact(&con);
			break;
		case SORT:
			//按照姓名排序个人信息
			SortContact(&con);
			break;
		case EXIT:
			DestoryContact(&con);
			printf("退出通讯录\n");
			break;
		default:
			printf("选择错误\n");
			break;
		}

	} while (input);
	return 0;
}