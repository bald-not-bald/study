#define _CRT_SECURE_NO_WARNINGS

#include<stdio.h>


//位段
//位段的声明和结构是类似的，有两个不同
//位段的成员必须是int，unsigned int，或者signed int
//位段的成员名后边有一个冒号和一个数字
//

//位段 - 位 - 二进制位

//struct S
//{
//	int _a : 2;
//	int _b : 5;
//	int _c : 10;
//	int _d : 30;
//};
//int main()
//{
//	printf("%zd\n", sizeof(struct S));
//	return 0;
//}

//位段的内存分配
//位段存在跨平台问题
struct S
{
	char a : 3;
	char b : 4;
	char c : 5;
	char d : 4;
};
int main()
{
	struct S s = { 0 };
	s.a = 10;
	s.b = 10;
	s.c = 10;
	s.d = 10;
	return 0;
}

