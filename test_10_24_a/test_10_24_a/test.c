#define _CRT_SECURE_NO_WARNINGS

//指针是内存中一个最小单元的标号，也就是地址
//编号->地址->指针
//通常口头中说的指针，通常指的是指针变量
#include<stdio.h>

//int main()
//{
//	int a = 10;
//	int* pa = &a;
//	printf("%p\n", &a);
//	printf("%p\n", pa);
//	return 0;
//
//}

//int main()
//{
//	int a = 0x11223344;
//	int* p = &a;
//
//	char* pc = &a;
//
//	return 0;
//}

//指针类型其实是有意义的
//1.指针类型决定了，指针进行解引用操作的时候，一次性访问几个字节
//如果是char*的指针，解引用访问1个字节
//如果是int*的指针，解引用访问4个字节
//float*--------------4个字节
//2.指针类型决定了指针的步长（指针+1跳过几个字节）
//字符指针+1，跳过一个字节
//整形指针+1,跳过4个字节
//

//int main()
//{
//	int a = 0x11223344;
//	char* pc = (char*)&a;
//	int i = 0;
//	for (i = 0; i < 4; i++)
//	{
//		*pc = 0;
//		pc++;
//	}
//	//int *p = &a;
//	return 0;
//}


//指针类型的意义
//指针的不同类型，其实提供了不同的视角去观看和访问内存

//野指针

//int main()
//{
//	int a = 10;
//	int* p = &a;
//	//int* p;//p是一个局部变量。没有初始化，里面是随机值
//	*p = 20;
//
//	return 0;
//}


//int main()
//{
//	int arr[] = { 1,2,3,4,5,6,7,8,9,10 };
//	int* p = arr;
//	int i = 0;
//	//int sz = sizeof(arr) / sizeof(arr[0]);
//
//	for (i = 0; i <= 10; i++)
//	{
//		printf("%d ", *p);//越界访问
//		p++;
//	}
//
//	return 0;
//}

//指针指向的空间释放
//int* test()
//{
//	int a = 10;
//	return &a;
//}
//int main()
//{
//	int* p = test();
//	printf("%d\n", *p);
//
//	return 0;
//}

//如何避免野指针
// 1.指针初始化
// 2.小心指针越界
// 3.指针指向空间释放，及时置NULL
// 4.避免返回局部变量的地址
// 5.使用指针前检查有效性
//

//int main()
//{
//	int a = 10;
//	int* p = &a;//指针的初始化
//
//	//int* p = NULL;//NULL - 空指针，专门用来初始化指针
//	//if (p != NULL)
//	//{
//
//	//}//搭配使用
//	//*p = 20;
//
//
//	return 0;
//}

//指针的运算
//#define N_VALUES 5
//float values[N_VALUES];
//float* vp;
//int main()
//{
//	for (vp = &values[0]; vp < &values[N_VALUES];)
//	{
//		*vp++ = 0;//先赋值0，再进行自增
//	}
//	return 0;
//}


//前提是两个指针指向同一块空间
//指针减去指针的绝对值
//得出的是指针和指针之间的元素个数

//int main()
//{
//	int arr[10] = { 0 };
//	printf("%d\n", &arr[9] - &arr[0]);
//
//
//	return 0;
//}

//写一个函数求一个字符串的长度
#include<string.h>
//int my_strlen(char* str)
//{
//	int count = 0;
//	while (*str != '\0')
//	{
//		count++;
//		str++;
//	}
//	return count;
//}

//int my_strlen(char* str)
//{
//	char* start = str;
//	while (*str != '\0')
//	{
//		str++;
//	}
//	return str - start;
//}
//
//int main()
//{
//	char arr[] = "abcdef";
//	int len = my_strlen(arr);
//
//	printf("%d\n", len);
//	return 0;
//}


//指针和数组
//1.指针和数组是不同的对象
// 指针是一种变量，存放地址的，大小4/8字节的
// 数组是一组相同类型元素的集合，是可以放多个元素的，大小取决于元素个数和元素的类型的
// 2.数组的数组名是数组首元素的地址，地址是可以放到指针变量里面的
// 
//

int main()
{
	int arr[10] = { 0 };
	int* p = arr;

	int i = 0;
	int sz = sizeof(arr) / sizeof(arr[0]);
	//赋值
	for (i = 0; i < sz; i++)
	{
		*p = i + 1;
		p++;
	}
	//打印
	p = arr;
	for (i = 0; i < sz; i++)
	{
		//printf("%d ", *p);
		//p++;
		//printf("%d ", *(arr + i));
		//printf("%d ", *(i+arr));
		//printf("%d ", i[arr]);
		printf("%d ", arr[i]);
	}
	return 0;
}


