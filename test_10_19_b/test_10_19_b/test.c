#define _CRT_SECURE_NO_WARNINGS

#include<stdio.h>
//int binary_search(int arr[], int k,int sz)//arr实际上是一个指针变量
//{
//
//	int left = 0;
//	int right = sz - 1;
//	while (left <= right)
//	{
//		int mid = left + (right - left) / 2;
//		if (arr[mid]<k)
//		{
//			left = mid + 1;
//		}
//		else if (arr[mid] >k)
//		{
//			right = mid - 1;
//		}
//		else
//		{
//			return mid;
//		}
//	}
//	return -1;
//}
//数组传参实际上传递的是数组首元素的地址
// 而不是整个数组
// 所以在函数内部计算一个函数参数部分的数组的元素个数是不靠谱的
// 
//int main()
//{
//	int arr[] = { 1,2,3,4,5,6,7,8,9,10 };
//	int k = 7;
//	int sz = sizeof(arr) / sizeof(arr[0]);
//	int ret = binary_search(arr, k,sz);
//	if (ret == -1)
//	{
//		printf("找不到\n");
//	}
//	else
//	{
//		printf("找到了，下标是:%d\n", ret);
//	}
//
//	return 0;
//}

//布尔类型
//int is_prime(int n)
//{
//	int j = 0;
//	for (j = 2; j <= sqrt(n); j+=2)
//	{
//		if (n % j == 0)
//		{
//			return false;
//		}
//	}
//	return true;
//}
//
//
//int main()
//{
//	int i = 0;
//	for (i = 100; i <= 200; i++)
//	{
//		if (is_prime(i))
//		{
//			printf("%d ", i);
//		}
//	}
//
//	return 0;
//}

//写一个函数，每调用一次这个函数，num就会增加1

//void Add(int* p)
//{
//	(*p)++;
//}
//
//
//int main()
//{
//	int num = 0;
//	Add(&num);
//	printf("%d\n", num);
//	
//	return 0;
//}

//函数的嵌套调用和连释访问
//函数可以嵌套调用但是不能嵌套定义
//void new_line()
//{
//	printf("hehe\n");
//}
//void three_line()
//{
//	int i = 0;
//	for (i = 0; i < 3; i++)
//	{
//		new_line();
//	}
//}
//int main()
//{
//	three_line();
//	return 0;
//}



//链式访问
#include<string.h>


//int main()
//{
//	int len = strlen("abcdef");
//	printf("%d\n", len);
//
//	//链式访问
//	printf("%d\n", strlen("abcdef"));
//	return 0;
//}

//链式访问的前提是函数有返回值


//int main()
//{
//	printf("%d", printf("%d", printf("%d", 43)));
//	return 0;
//}


//函数不写返回值的时候，默认返回类型是int
// 不推荐
//Add(int x, int y)
//{
//	return x + y;
//}


//明确的说明，main函数不需要参数
//本质上main函数是有参数的

//int main(void)
//{
//
//	return 0;
//}


//main函数有3个参数
//int main(int argc, char* argv[], char* envp[])
//{
//	return 0;
//}








