#define _CRT_SECURE_NO_WARNINGS

#include<stdio.h>
#include<string.h>

//自定义类型的详解
//结构体

//typedef struct Stu
//{
//	char name[20];
//	int age;
//}Stu;//全局变量，s1和s2是两个结构体变量
//
//int main()
//{
//	struct Stu s3, s4;//局部变量
//	Stu s5, s6;
//	
//	return 0;
//}

//匿名结构体类型
//struct
//{
//	char name[20];
//	int age;
//}x;
//struct
//{
//	int a;
//	char c;
//	double d;
//}*p;
//int main()
//{
//	p = &x;//非法
//	return 0;
//}

//结构体的自引用
//typedef struct Node
//{
//	int data;
//	struct Node* next;
//}Node;
//
//int main()
//{
//	struct Node n1;
//	struct Node n2;
//
//	return 0;
//}

//struct Point
//{
//	int x;
//	int y;
//}p1 = {10,20};
//struct Point p2 = { 0,0 };
//struct S
//{
//	int num;
//	char ch;
//	struct Point p;
//	float d;
//};
//
//int main()
//{
//	struct Point p3 = { 1,2 };
//	struct S s1 = { 100,'w',{2,5},3.14f };
//	struct S s2 = { .d = 1.2f,.p.x = 3,.p.y = 5,.ch = 'q',.num = 200 };;
//	printf("%d %c %d %d %f\n", s1.num, s1.ch, s1.p.x, s1.p.y, s1.d);
//	printf("%d %c %d %d %f\n", s2.num, s2.ch, s2.p.x, s2.p.y, s2.d);
//
//	return 0;
//}

//结构体内存对齐
//结构体的第一个成员，对齐到结构体在内存中存放位置的0偏移处
//从第二个成员开始，每个成员都要对齐到（一个对齐数）的整数倍处
//对齐数：结构体成员自身大小和默认对齐数的较小值
//VS：默认对齐数8
//Linux gcc：没有默认对齐数，对齐数就是结构体成员的自身大小
//结构体的总大小，必须是所有成员的对齐数中最大对齐数的整数倍
//如果结构体中，嵌套了结构体成员，要将嵌套的结构体成员对齐到自己的成员中最大对齐数的整数倍数处
#include<stddef.h>
//struct S1
//{
//	char c1;
//	int i;
//	char c2;
//};
//
//struct S2
//{
//	char c1;
//	char c2;
//	int i;
//};
//struct S3
//{
//	double d;
//	char c;
//	int i;
//};
//struct S4
//{
//	char c1;
//	struct S3 s3;
//	double d;
//};
//int main()
//{
//	printf("%zd\n", sizeof(struct S1));
//	printf("%zd\n", sizeof(struct S2));
//	printf("%zd\n", sizeof(struct S3));
//	printf("%zd\n", sizeof(struct S4));
//	printf("%zd\n", offsetof(struct S1, c1));
//	printf("%zd\n", offsetof(struct S1, i));
//	printf("%zd\n", offsetof(struct S1, c2));
//	return 0;
//}

//修改默认对齐数
//VS环境下默认对齐数是8
//#pragma comment()
//#pragma pack(1)
//struct S1
//{
//	char c1;
//	char c2;
//	int i;
//};
//struct S2
//{
//	char c1;
//	int i;
//	char c2;
//};
//
////#pragma pack()
//
//int main()
//{
//	printf("%zd\n", sizeof(struct S1));
//	printf("%zd\n", sizeof(struct S2));
//	return 0;
//}


//函数传参的时候，参数是需要压栈的，会有时间和空间上的系统开销
//如果传递一个结构体对象的时候，结构体过大，参数压栈的系统开销比较大，所以会导致性能的下降
