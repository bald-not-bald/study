#define _CRT_SECURE_NO_WARNINGS

#include<stdio.h>

//课后作业

//ASCII码

//int main()
//{
//	int arr[] = { 73,32,99,97,110,32,100,111,35,105,116,33 };
//	int i = 0;
//	//sizeof(arr) - 计算数组的总大小
//	//sizeof(arr[0]) - 计算数组元素的单个大小
//	int sz = sizeof(arr) / sizeof(arr[0]);
//	while(i<sz)
//	{
//		printf("%c", arr[i]);
//		i++;
//	}
//	return 0;
//}


//出生日期输入输出
//int main()
//{
//	//输入数据
//	int year = 0;
//	int month = 0;
//	int date = 0;
//	scanf("%4d%2d%2d", &year, &month, &date);
//
//
//	//输出
//	printf("year=%d\n", year);
//	printf("month=%02d\n", month);
//	printf("date=%02d\n", date);
//
//	return 0;
//}


//学生信息的输入输出


//int main()
//{
//	int id = 0;
//	float c = 0.0f;
//	float math = 0.0f;
//	float Eng = 0.0f;
//	scanf("%d;%f,%f,%f", &id, &c, &math, &Eng);
//	printf("The each subject score of No. %d is %.2f,%.2f,%.2f\n", id, c, math, Eng);
//
//	return 0;
//}


//printf的返回值
//int main()
//{
//	int n = printf("Hello world!");
//	printf("\n%d\n", n);
//
//	return 0;
//}


//int sum(int a)
//{
//	int c = 0;
//	static int b = 3;
//	c += 1;
//	b += 2;
//	return(a + b + c);
//
//}
//
//int main()
//{
//	int i;
//	int a = 2;
//	for (i = 0; i < 5; i++)
//	{
//		printf("%d,", sum(a));
//	}
//	return 0;
//}

//KIKI学程序设计基础

//int main()
//{
//	printf("printf\"Hello world\\n\";\n");
//	printf("cout<<\"Hello world!\"<<endl;\n");
//	return 0;
//}



//小乐乐找最大数
//int main()
//{
//	//输入
//	int arr[4] = { 0 };
//	int i = 0;
//	for (i = 0; i < 4; i++)
//	{
//		scanf("%d", &arr[i]);
//	}
//	//假设最大值
//	int max = arr[0];
//	i = 1;
//	for (i = 1; i < 4; i++)
//	{
//		if (arr[i] > max)
//		{
//			max = arr[i];
//		}
//	}
//	printf("%d\n", max);
//	return 0;
//}

//计算球的体积

//int main()
//{
//	double r = 0.0;
//	double v = 0.0;
//
//	scanf("%lf", &r);
//	v = 4 / 3.0 * 3.1415926 * r * r * r;
//
//	printf("%.3lf\n", v);
//	return 0;
//}

//0.0默认是double类型


//计算体重指数

//int main()
//{
//	int weight = 0;
//	int height = 0;
//	scanf("%d %d", &weight, &height);
//
//	//计算
//	float bmi = weight / (height / 100.0) / (height / 100.0);
//
//	//输出
//	printf("%2f", bmi);
//	return 0;
//}

//int main()
//{
//	int i = 1;//初始化
//
//	while (i <= 10)//判断
//	{
//		printf("%d ", i);
//		i++;//调整
//	}
//
//	return 0;
//}

//int main()
//{
//	int i = 0;
//	for (i = 1; i <= 10; i++)
//	{
//		if (5 == i)
//			continue;
//		printf("%d ",i);
//
//	}
//	return 0;
//}

//int main()
//{
//	//int arr[10] = { 1,2,3,4,5,6,7,8,9,10 };
//
//	//int i = 0;
//	//for (i = 0; i < 10; i++)
//	//{
//	//	printf("%d ", arr[i]);
//	//}
//
//
//	//如果for循环的判断部分省略意味着判断会恒成立
//	//for (;;)
//	//{
//	//	printf("hehe\n");
//	//}
//	//int i = 0;
//	//for (i = 0; i < 10; i++)
//	//{
//	//	printf("hehe\n", i);
//	//}
//	int i = 0;
//	int j = 0;
//	for (i = 0; i < 3; i++)
//	{
//		for (j = 0; j < 3; j++)
//		{
//			printf("hehe\n");
//		}
//	}
//
//
//	return 0;
//}


//int main()
//{
//	int i = 0;
//	int k = 0;
//	for (i = 0, k = 0; k = 0; i++, k++)//k被赋值为0，条件为假，循环次数为0
//		k++;
//	return 0;
//}

//int main()
//{
//	int i = 0;
//	do
//	{
//		i++;
//		if (5 == i)
//			continue;
//		printf("%d ", i);
//
//	}
//	while (i <= 10);
//
//	return 0;
//}


//计算n的阶乘

//int main()
//{
//	int n = 0;
//	scanf("%d", &n);
//	int i = 0;
//	int mix = 1;
//
//
//	for (i = 1; i <= n; i++)
//	{
//		mix *= i;
//	}
//	printf("%d\n", mix);
//	return 0;
//}

//计算阶乘的和

//int main()
//{
//	int j = 1;
//	int ret = 1;
//	int mix = 0;
//	for (j = 1; j <= 10; j++)
//		{
//			ret *= j;
//			mix += ret;
//		}
//	printf("%d\n", mix); 
//	return 0;
//}

//在一个有序数组中查找一个数的下标
//int main()
//{
//	int arr[] = { 1,2,3,4,5,6,7,8,9,10 };
//	int k = 7;
//	int sz = sizeof(arr) / sizeof(arr[0]);
//	int i = 0;
//	for (i = 0; i < sz; i++)
//	{
//		if (arr[i] == k)
//		{
//			printf("找到了，下标是: %d\n", i);
//			break;
//		}
//	}
//	if (i == sz)
//	{
//		printf("找不到\n");
//	}
//	return 0;
//}


//折半查找
//二分查找

int main()
{
	int arr[] = { 1,2,3,4,5,6,7,8,9,10 };
	int k = 7;
	int sz = sizeof(arr) / sizeof(arr[0]);

	int left = 0;
	int right = sz - 1;

	while (left<=right)
	{
		int mid = (left + right) / 2;
		if (arr[mid] < k)
		{
			left = mid + 1;
		}
		else if (arr[mid] > k)
		{
			right = mid - 1;
		}
		else
		{
			printf("找到了，下标是:%d\n", mid);
			break;
		}
		
	}
	if (left > right)
	{
		printf("找不到\n");
	}

	return 0;
}



