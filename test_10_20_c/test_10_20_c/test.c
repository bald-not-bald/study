#define _CRT_SECURE_NO_WARNINGS

#include<stdio.h>
#include<string.h>
//int Fun(int n)
//{
//	if (5 == n)
//		return 2;
//	else
//		return 2 * Fun(n + 1);
//}
//int main()
//{
//	int ret = Fun(2);
//	printf("%d", ret);
//	return 0;
//}

//字符串逆序(递归实现）
//int my_strlen(char arr[])
//{
//	int count = 0;
//	while (*arr != '\0')
//	{
//		count++;
//		arr++;
//	}
//	return count;
//}

//void reverse(char arr[])
//{	
//	char tmp = *arr;
//	int len = my_strlen(arr);
//	*arr = *(arr + len - 1);
//	*(arr+len - 1) = '\0';
//	if(my_strlen(arr+1)>=2)
//		reverse(arr + 1);
//	*(arr + len - 1) = tmp;
//}


//int main()
//{
//	char arr[] = "abcdef";
//	reverse(arr);
//	printf("%s\n", arr);
//	return 0;
//}


//void reverse(char arr[],int left,int right)
//{
//	char tmp = arr[left];
//	arr[left] = arr[right];
//	arr[right] = tmp;
//	if (left < right)
//	{
//		reverse(arr, left + 1, right - 1);
//	}
//
//}
//
//int main()
//{
//	char arr[] = "abcdefg";
//	int left = 0;
//	int right = my_strlen(arr) - 1;
//	reverse(arr, left, right);
//	printf("%s\n", arr);
//	return 0;
//}


//void reverse(char arr[])
//{
//	int left = 0;
//	//int right = sz - 2;
//	int right = strlen(arr) - 1;
//	while (left < right)
//	{
//		char tmp = arr[left];
//		arr[left] = arr[right];
//		arr[right] = tmp;
//		left++;
//		right--;
//	}
//}
//
//int main()
//{
//	char arr[] = "abcdef";
//	int sz = sizeof(arr) / sizeof(arr[0]);
//	reverse(arr);
//	printf("%s\n", arr);
//	return 0;
//}

//计算一个数的每位之和
//int Digitsum(unsigned int n)
//{
//	if (n > 9)
//	{
//		return Digitsum(n / 10) + n % 10;
//	}
//	else
//		return n;
//}
//
//int main()
//{
//	unsigned int n = 0;
//	scanf("%u", &n);
//	int sum = Digitsum(n);
//	printf("%d", sum);
//
//	return 0;
//}

//编写一个函数实现n的k次方，使用递归实现
//double Pow(int n, int k)
//{
//	if (k > 0)
//		return Pow(n, k - 1) * n;
//	else if (k == 0)
//		return 1;
//	else
//		return 1.0 / Pow(n, -k);
//}
//
//int main()
//{
//	int n = 0;
//	int k = 0;
//	scanf("%d %d", &n, &k);
//	double ret = Pow(n,k);
//
//	printf("%lf\n", ret);
//	return 0;
//}

//int main()
//{
//	int num = 10;
//	int arr[10] = { 0 };
//	printf("%d\n", sizeof(arr));
//	printf("%d\n", sizeof(arr[10]));
//	
//	return 0;
//}

//sizeof是一个操作符
//是用来计算变量（类型）所占内存空间的大小，不关系内存中存放的具体内容
//但是是字节

//strlen
// strlen是一个库函数，是专门求字符串长度的，只能针对字符串
// 从参数给定的地址向后一直找\0，统计\0之前出现的字符的个数
//
//int main()
//{
//	char str[] = "hello bit";
//
//	printf("%d %d\n", sizeof(str), strlen(str));
//	return 0;
//}

//将数组A中的内容和数组B中的内容进行交换（数组一样大）

//int main()
//{
//	int arr1[] = { 1,2,5,7,9 };
//	int arr2[] = { 2,4,6,8,10 };
//	int i = 0;
//	int sz = sizeof(arr1) / sizeof(arr1[0]);
//	for (i = 0; i < sz; i++)
//	{
//		int tmp = arr1[i];
//		arr1[i] = arr2[i];
//		arr2[i] = tmp;
//	}
//	for (i = 0; i < sz; i++)
//	{
//		printf("%d\ ", arr1[i]);
//	}
//	printf("\n");
//	for (i = 0; i < sz; i++)
//	{
//		printf("%d\ ", arr2[i]);
//	}
//	printf("\n");
//	return 0;
//}

//创建一个整型数组，完成对数组的操作
// 实现函数init（）初始化数组位全0
// 实习print（）打印数组的每个元素
// 实现reverse（）函数实现数组元素的逆置
//
void init(int arr[], int sz)
{
	int i = 0;
	for (i = 0; i < sz; i++)
	{
		arr[i] = 0;
	}
}

void print(int arr[], int sz)
{
	int i = 0;
	for (i = 0; i < sz; i++)
	{
		printf("%d ", arr[i]);
	}
	printf("\n");
}

void reverse(int arr[], int sz)
{
	int left = 0;
	int right = sz - 1;
	while (left < right)
	{
		int tmp = arr[left];
		arr[left] = arr[right];
		arr[right] = tmp;
		left++;
	}
}

int main()
{
	int arr[10] = { 1,2,3,4,5,6,7,8,9,0 };
	int sz = sizeof(arr) / sizeof(arr[0]);
	print(arr, sz);
	
	reverse(arr, sz);
	print(arr, sz);

	init(arr, sz);
	print(arr, sz);

	return 0;
}



