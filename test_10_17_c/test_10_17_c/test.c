#define _CRT_SECURE_NO_WARNINGS


#include<stdio.h>


//C语言是结构化的计算机语言
// 
// 
//

//int main()
//{
	//int a = 0;
	//if (3 == 5)
	//	printf("hehe\n");
	//int age = 18;
	//if (age > 18)
	//	printf("成年\n");

	//int age = 10;
	//if (age < 18)
	//{
	//	printf("未成年\n");
	//	printf("不能饮酒\n");
	//}
	//else 
	//{
	//	printf("成年\n");
	//}

//	int age = 0;
//	scanf("%d", &age);
//
//
//	if (age < 18)
//		printf("青少年\n");
//	else if (age >= 18 && age < 28)
//		printf("青年\n");
//	else if (age >= 28 && age < 40)
//		printf("中年\n");
//	else if (age >= 40 && age < 60)
//		printf("壮年\n");
//	else if (age >= 60 && age < 100)
//		printf("老年\n");
//	else
//		printf("老寿星\n");
//
//	return 0;
//}


//int main()
//{
//	int age = 20;
//	if (age < 18)
//		printf("未成年\n");
//	else
//	{
//		printf("成年\n");
//		printf("打游戏\n");
//	}
//	return 0;
//}



//else是和最近的if匹配的
//int main()
//{
//	int a = 0;
//	int b = 2;
//	if (a == 1)
//	{
//		if (b == 2)
//			printf("hehe\n");
//		else
//			printf("haha\n");
//	}
//	return 0;
//}

//1.变量的命名（有意义，规范）
//2.空格，空行，换行
// 
//

//int main()
//{
//	char first_name[20] = { 0 };
//
//
//
//	return 0;
//}

//int test()
//{
//	int a = 3;
//	if (a == 3)
//		return 1;
//	return 0;
//}


//int test()
//{
//	int a = 3;
//	if (a == 3)
//		return 1;
//	else
//		return 0;
//}
//
//
//int main()
//{
//	int r = test();
//	printf("%d\n", r);
//
//	return 0;
//}

//int main()
//{
//	int num = 3;
//	if (5 == num)//if(num == 5)
//		printf("hehe\n");
//
//	return 0;
//}


//int main()
//{
//	//判断一个数是奇数还是偶数
//	int a = 0;
//	scanf("%d", &a);
//	if (a % 2 == 0)
//		printf("偶数\n");
//	else
//		printf("奇数\n");
//
//
//	return 0;
//}

//输出1-100之间的奇数


//int main()
//{
//	int a = 1;
//	while (a < 101)
//	{
//		if (a % 2 == 1)
//		{
//			printf("%d ", a);
//		}
//		a++;
//	}
//	return 0;
//}

//switch语句


//int main()
//{
//	int day = 0;
//	scanf("%d", &day);
//	//if (1 == day)
//	//	printf("星期一\n");
//	switch (day)
//	{
//	case 1:
//		printf("星期一\n");
//		break;
//	case 2:
//		printf("星期二\n");
//		break;
//	case 3:
//		printf("星期三\n");
//		break;
//	case 4:
//		printf("星期四\n");
//		break;
//	case 5:
//		printf("星期五\n");
//		break;
//	case 6:
//		printf("星期六\n");
//		break;
//	case 7:
//		printf("星期日\n");
//		break;
//	}
//
//
//
//	return 0;
//}

//int main()
//{
//	int day = 0;
//	scanf("%d", &day);
//	//if (1 == day)
//	//	printf("星期一\n");
//	switch (day)
//	{
//	case 1:
//	case 2:
//	case 3:
//	case 4:
//	case 5:
//		printf("工作日\n");
//		break;
//	case 6:
//	case 7:
//		printf("休息日\n");
//		break;
//	default:
//		printf("选择错误\n");
//		break;
//
//	}
//
//	return 0;
//}


int main()
{
	int n = 1;
	int m = 2;
	switch (n)
	{
	case 1:m++;
	case 2:n++;
	case 3:
		switch (n)
		{
		case 1:n++;
		case 2:m++;
			break;
		}
	case 4:
		m++;
		break;
	default:
		break;
	}
	printf("m = %d, n = %d", m, n);
	return 0;
}



