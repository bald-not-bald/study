#define _CRT_SECURE_NO_WARNINGS


#include<stdio.h>

//数组


//int main()
//{
//	//数组是一组相同类型元素的集合
//	int arr[10];
//	char ch[5];
//	double data[20];
//
//
//	return 0;
//
//}

//一维数组的使用

//int main()
//{
//	int arr[] = { 1,2,3,4,5,6,7,8,9 };
//	//[]下标引用操作符
//	//printf("%d\n",arr[4]);
//	int i = 0;
//	int sz = sizeof(arr) / sizeof(arr[0]);
//	//打印数组的每个元素的地址
//	//数组在内存中是连续存放的
//	//for (i = 0; i < sz; i++)
//	//{
//	//	printf("&arr[%d] = %p\n", i, &arr[i]);
//	//}
//	//for (i = 0; i < sz; i++)
//	//{
//	//	printf("%d\n", arr[i]);
//	//}
//	//for (i = sz - 1; i >= 0; i--)
//	//{
//	//	printf("%d\n", arr[i]);
//	//}
//
//
//	return 0;
//}

//int main()
//{
//	//int arr[3][4] = { 1,2,3,4,2,3,4,5,3,4,5,6 };
//	//int arr[3][4] = { {1,2},{2,3},{3,4} };
//	//char ch1[5][10];
//	int arr[3][4] = { 1,2,3,4,2,3,4,5,3,4,5,6 };
//	int i = 0;
//	for (i = 0; i < 3; i++)
//	{
//		int j = 0;
//		for (j = 0; j < 4; j++)
//		{
//			printf("%d ", arr[i][j]);
//		}
//		printf("\n");
//	}
//
//
//	return 0;
//}


//数组越界
//int main()
//{
//	int arr[] = {1,2,3,4,5,6 };
//	int i = 0;
//	int sz = sizeof(arr) / sizeof(arr[0]);
//	for (i = 0; i < sz; i++)
//	{
//		printf("%d ",arr[i]);
//	}
//	return 0;
//
//}

//int main()
//{
//	int arr[3][4] = { 1,2,3,4,1,2,3,4,1,2,3,4 };
//	int i = 0;
//	for (i = 0; i < 3; i++)
//	{
//		int j = 0;
//		for (j = 0; j < 5; j++)
//		{
//			printf("%d ", arr[i][j]);
//		}
//		printf("\n");
//	}
//	return 0;
//}
//数组传参的时候，有两种写法:
//1.数组
//2.指针

//一趟冒泡排序让一个数据来到它最终应该出现的位置上
//void bubble_sort(int arr[],int sz)
//{
//	//确定冒泡排序使用的趟数
//	int i = 0;
//	for (i = 0; i < sz - 1; i++)
//	{
//		int j = 0;
//		for (j = 0; j < sz-1-i; j++)
//		{
//			if (arr[j] > arr[j + 1])
//			{
//				//交换
//				int tmp = arr[j];
//				arr[j] = arr[j + 1];
//				arr[j + 1] = tmp;
//			}
//		}
//	}
//}
//
//int main()
//{
//	int arr[] = { 9,8,7,6,4,5,3,2,1 };
//	//冒泡排序的算法，对数组进行排序
//	int sz = sizeof(arr) / sizeof(arr[0]);
//	bubble_sort(arr,sz);
//	int i = 0;
//	for (i = 0; i < sz; i++)
//	{
//		printf("%d", arr[i]);
//	}
//	return 0;
//
//}

//数组名是什么
//数组名确实能表示首元素的地址
//但是有两个例外
//1.sizeof（数组名），这里的数组名表示整个数组，计算的是整个数组的大小，单位是字节
//2.&数组名，这里的数组名表示整个数组，取出的是整个数组的地址
//int main()
//{
//	int arr[10] = { 0 };
//	printf("%p\n", arr);//arr就是首元素的地址
//	printf("%p\n", arr+1);
//	printf("----------------------------\n");
//	printf("%p\n", &arr[0]);//首元素的地址
//	printf("%p\n", &arr[0]+1);
//	printf("----------------------------\n");
//	printf("%p\n", &arr);//数组的地址
//	printf("%p\n", &arr+1);
//
//	int n = sizeof(arr);
//	printf("%d\n", n);
//
//	return 0;
//}


//二维数组的数组名的理解
int main()
{
	int arr[3][4];
	int sz = sizeof(arr);

	printf("%d\n", sizeof(arr) / sizeof(arr[0]));//行
	printf("%d\n",sizeof(arr[0]) / sizeof(arr[0][0]));//列
	//printf("%d\n", sz);

	//arr;//二维数组的数组名也表示数组首元素的地址


	return 0;
}









