#define _CRT_SECURE_NO_WARNINGS

#include<stdio.h>
#include<string.h>
//1.sizeof（数组名），数组名表示整个书组织，计算的是整个数组的大小，单位是字节
//2.&数组名，数组名表示整个数组。取出的是整个数组的地址
//除此之外，所有的数组名都是数组首元素的地址
//int main()
//{
//	int a[] = { 1,2,3,4 };
//	printf("%zd\n", sizeof(a));//16
//	printf("%zd\n", sizeof(a+0));//a+0 其实是数组第一个元素的地址，是4或8字节
//	printf("%zd\n", sizeof(*a));//*a是数组首元素，计算的是数组首元素的大小，单位是字节，4
//	printf("%zd\n", sizeof(a+1));//a+1是第二个元素的地址，是4/8字节
//	printf("%zd\n", sizeof(a[1]));//a[1]是第二个元素。计算的是第二个元素的大小 - 4单位是字节
//	printf("%zd\n", sizeof(&a));//&a是整个数组的地址，整个数组的地址也是地址，地址的大小就是4/8字节
//	//&a-->类型:int(*)[4]
//
//	printf("%zd\n", sizeof(*&a));//&a是数组的地址,*&a就是拿到了数组，*&a-->a，a就是数组名，sizeof(*&a)-->sizeof(a)
//	//计算的是整个数组的大小,16字节
//	printf("%zd\n", sizeof(&a+1));//&a+1跳过整个数组，指向数组后面的空间，仍然是地址，大小是4/8字节
//	printf("%zd\n", sizeof(&a[0]));//&a[0]就是首元素地址，计算首元素地址大小，4/8字节
//	printf("%zd\n", sizeof(&a[0]+1));//计算第二个元素地址大小，4/8字节
//	return 0;
//}

//int main()
//{
//	char arr[] = {'a','b','c','d','e','f'};
//	printf("%zd\n", strlen(arr));//随机值
//	printf("%zd\n", strlen(arr + 0));//随机值
//	//printf("%zd\n", strlen(*arr));//strlen('a')->strlen(97),非法访问-err
//	//printf("%zd\n", strlen(arr[1]));//类似上面，非法访问
//	printf("%zd\n", strlen(&arr));//&arr虽然是数组的地址，但是也是从数组起始位置开始的，计算的还是随机值
//	printf("%zd\n", strlen(&arr + 1));//同上
//	printf("%zd\n", strlen(&arr[0] + 1));//
//
//	//printf("%zd\n", sizeof(arr));//arr单独放在sizeof内部，计算的是整个数组的大小，6
//	//printf("%zd\n", sizeof(arr+0));//arr+0 其实是数组第一个元素的地址，是4或8字节
//	//printf("%zd\n", sizeof(*arr));//*arr是数组首元素，计算的是数组首元素的大小，单位是字节，1
//	//printf("%zd\n", sizeof(arr[1]));//arr[1]是第二个元素。计算的是第二个元素的大小 - 1单位是字节
//	//printf("%zd\n", sizeof(&arr));//&arr是整个数组的地址，整个数组的地址也是地址，地址的大小就是4/8字节
//	//printf("%zd\n", sizeof(&arr+1));//&arr+1跳过整个数组，指向数组后面的空间，仍然是地址，大小是4/8字节
//	//printf("%zd\n", sizeof(&arr[0]+1));//计算第二个元素地址大小，4/8字节
//	return 0;
//}

//sizeof只关注占用内存空间的大小，单位是字节，不关心内存中华存放的是什么
//strlen是求字符串长度的，统计的是\0之前出现的字符个数，一定要找到\0才结束，所以可能出现越界访问


//int main()
//{
//	const char* p = "abcdef";
//	printf("%d\n", sizeof(p));//p是指针变量，大小是4/8字节
//	printf("%d\n", sizeof(p+1));//p+1是b的地址，地址就是4/8字节
//	printf("%d\n", sizeof(*p));//*p是'a'，sizeof（*p）计算的是字符的大小，是1字节
//	printf("%d\n", sizeof(p[0]));//p[0]-->*(p+0)-->*p 就同上一个，1字节
//	printf("%d\n", sizeof(&p));//&p是二级指针，是指针大小就是4/8
//	printf("%d\n", sizeof(&p+1));//&p+1是跳过p变量后的地址，4/8字节
//	printf("%d\n", sizeof(&p[0]));//p[0]就是'a',&p[0]就是a的地址，+1,就是b的地址，是地址就是4/8;
//
//	return 0;
//}

//int main()
//{
//	printf("%zd\n", sizeof('a'));//
//	return 0;
//}


//int main()
//{
//	const char* p = "abcdef";
//
//	printf("%zd\n", sizeof(p));//6-求字符串长度
//	printf("%zd\n", sizeof(p+1));//p+1是b的地址，求字符串长度就是5
//	//printf("%zd\n", sizeof(*p));//err
//	//printf("%zd\n", sizeof(p[0]));//非法
//	printf("%zd\n", sizeof(&p));//&p拿到的是p这个指针变量的起始地址，从这里开始求字符串长度完全是随机值
//	printf("%zd\n", sizeof(&p+1));//&p+1是跳过p变量的地址，从这里开始求字符串长度也是随机值
//	printf("%zd\n", sizeof(&p[0]+1));//&p[0]+1是b的地址，从b的地址向后数字符串的长度是5
//
//	return 0;
//}


//int main()
//{
//	int a[3][4] = { 0 };
//
//	printf("%zd\n", sizeof(a));//48 = 3*4*4 整个数组的大小
//	printf("%zd\n", sizeof(a[0][0]));//4
//	printf("%zd\n", sizeof(a[0]));//a[0]是第一行的数组名，数组名单独放在sizeof内部，计算的就是数组（第一行）的大小，16个字节
//	printf("%zd\n", sizeof(a[0]+1));//a[0]作为第一行的数组名没有单独放在sizeof内部，没有取地址，表示的就是数组首元素的地址
//	//那就是a[0][0]的地址 a[0]+1就是第一行第二个元素的地址，是地址就是4/8个字节
//	printf("%zd\n", sizeof(*(a[0]+1)));//第一行第二个元素的大小，4个字节
//	printf("%zd\n", sizeof(a+1));//数组名表示首元素的地址，就是第一行的地址，a+1就是第二行的地址
//	//第二行的地址也是地址，是地址就是4/8  a+1-->int(*)[4]
//	//a - int(*)[4]
//	//a+1 - int(*)[4]
//	printf("%zd\n", sizeof(*(a+1)));//求的是整个第二行的大小，4*4=16
//	printf("%zd\n", sizeof(&a[0]+1));//8  &a[0]是第一行的地址，+1就是第二行的地址
//	printf("%zd\n", sizeof(*(&a[0]+1)));//16  对第二行的地址解引用，得到的就是第二行，计算的就是第二行的大小
//	printf("%zd\n", sizeof(*a));//16  a是数组首元素的地址，就是第一行的地址，*a就是第一行，计算的就是第一行的大小
//	//*a - *(a+0) - a[0]
//	printf("%zd\n", sizeof(a[3]));//类型是int[4]
//	//16 如果数组存在第四行，a[3]就是第四行的数组名，大小就是16字节
//	//
//
//	return 0;
//}

//放在sizeof内部不参与计算
//int main()
//{
//	short s = 3;
//	int a = 10;
//
//	printf("%zd\n", sizeof(s = a + 2));//2
//	printf("%d\n", s);
//	return 0;
//}

//C语言中表达式有两个属性
//2+3
//值属性：5
//类型属性：int


