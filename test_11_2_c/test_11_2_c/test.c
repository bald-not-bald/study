#define _CRT_SECURE_NO_WARNINGS

#include<stdio.h>
//函数指针 - 指向函数的指针
int Add(int x, int y)
{
	return x + y;
}
int main()
{
	int (*pf)(int, int) = &Add;
	int sum = (*pf)(2, 3);
	printf("%d\n", sum);
	//&函数名和函数名都是函数的地址
	//
	//pf是一个存放函数地址的指针变量 - 函数指针
	return 0;

}