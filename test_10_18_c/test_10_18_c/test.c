#define _CRT_SECURE_NO_WARNINGS

#include<stdio.h>
#include<string.h>
#include<windows.h>
#include<stdlib.h>

//int main()
//{
//	int arr[] = { 1,2,3,4,5,6,7,8,9,10 };
//	int sz = sizeof(arr) / sizeof(arr[0]);
//	int k = 7;
//	int left = 0;
//	int right = sz-1;
//
//	while (left <= right)
//	{
//		//int mid = (left + right) / 2;
//		int mid = left + ((right - left) / 2);
//		if (k > arr[mid])
//		{
//			left = mid + 1;
//		}
//		else if (k < arr[mid])
//		{
//			right = mid - 1;
//		}
//		else
//		{
//			printf("找到了,下标是:%d\n", mid);
//			break;
//		}
//	}
//	if (left > right)
//	{
//		printf("找不到\n");
//	}
//
//	return 0;
//}


//便携代码，演示多个字符从两端移动，向中间汇聚
//welcome to bit!!!!
//##################
//w################!
//we##############!!
//......
//welcome to bit!!!!
//
//int main()
//{
//	char arr1[] = "welcome to bit!!!!";
//	char arr2[] = "##################";
//	int right = strlen(arr1)-1;
//	int left = 0;
//	//过程
//	while (left<=right)
//	{
//		arr2[left] = arr1[left];
//		arr2[right] = arr1[right];
//		printf("%s\n", arr2);
//		//增加生成间隔
//		Sleep(1000);
//		//清空屏幕
//		system("cls");//system是一个库函数,可以执行系统命令
//
//		left++;
//		right--;
//	}
//	printf("%s\n", arr2);
//
//	return 0;
//}


//编写代码实现，模拟用户登陆，并且只能登录三次（只允许输入三次密码），如果密码正确
//提示登录成功，如果三次均输入错误，则跳出程序
//int main()
//{
//	int i = 0;
//	char password[20] = { 0 };
//	for (i = 0; i < 3; i++)
//	{
//		printf("请输入密码:>");
//		scanf("%s", password);
//		if (strcmp(password,"abcdef") == 0)//比较两个字符串是否相等不能使用等号，而应该使用库函数：strcmp
//		{
//			printf("登录成功\n");
//			break;
//		}
//		else
//		{
//			printf("密码错误\n");
//		}
//	}
//	if (3 == i)
//	{
//		printf("三次密码均输入错误，退出程序");
//	}
//	return 0;
//}

//猜数字游戏
//电脑生成一个随机数（1-100）
// 猜数字
// 如果猜测的数字大于生成数
// 反馈大
// 反之则反馈小
// 知道猜对
//

//#include<stdio.h>
//#include<stdlib.h>
//#include<time.h>
//
//void menu()
//{
//	printf("******************************\n");
//	printf("*********    1.play    *******\n");
//	printf("*********    0.exit    *******\n");
//	printf("******************************\n");
//}
//
//void game()
//{
//	int guess = 0;
//	//1.生成随机数
//	int ret = rand()%100+1;//生成随机数的函数
//	//printf("%d\n", ret);
//	//2.猜数字
//	while (1)
//	{
//		printf("请猜数字:>");
//		scanf("%d", &guess);
//		if (guess < ret)
//		{
//			printf("猜小了\n");
//		}
//		else if (guess > ret)
//		{
//			printf("猜大了\n");
//		}
//		else
//		{
//			printf("恭喜你,猜对了\n");
//			break;
//		}
//	}
//}
//
////指针 int* p = NULL;
////int a = 0;
//
//
//int main()
//{
//	int input = 0;
//	srand((unsigned int)time(NULL));
//	do
//	{
//		menu();
//		printf("请选择:");
//		scanf("%d", &input);
//		switch (input)
//		{
//		case 1:
//			game();//猜数字
//			break;
//		case 0:
//			printf("退出游戏\n");
//			break;
//		default:
//			printf("选择错误，重新选择\n");
//			break;
//		}
//	} while (input);
//
//	return 0;
//}


//goto语句

//int main()
//{
//again:
//	printf("hehe\n");
//	printf("hehe\n");
//	goto again;
//	return 0;
//}


//关机程序
//1.电脑运行起来后一分钟内关机
//2.如果输入我是猪就取消关机
//

//int main()
//{
//	char input[20] = { 0 };
//	system("shutdown -s -t 6000");
//again:
//	printf("请注意，你的电脑在60秒内关机，如果输入:我是猪，就取消关机\n");
//	scanf("%s", input);
//	if (strcmp(input, "我是猪") == 0)
//	{
//		system("shutdown -a");
//	}
//	else
//	{
//		goto again;
//	}
//	return 0;
//}


//




