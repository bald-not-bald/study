#define  _CRT_SECURE_NO_WARNINGS 1

#include<stdio.h>
#include<string.h>
#include<stdlib.h>


//常见的动态内存错误

//1.对空指针的解引用操作
//int main()
//{
//	//int* p = (int*)realloc(NULL, 40);//等效于malloc(40);
//	int *p = (int*)malloc(100);
//	//一定要判断malloc的返回值
//	if (p == NULL)
//	{
//		return 1;
//	}
//	int i = 0;
//	for (i = 0; i < 20; i++)
//	{
//		*(p + i) = 0;
//	}
//
//	return 0;
//}


//2.对动态内存开辟的空间越界访问
//int main()
//{
//	int* p = (int*)malloc(100);
//	if (NULL == p)
//	{
//		return 1;
//	}
//	int i = 0;
//	for (i = 0; i < 100; i++)
//	{
//		//*(p + i) = 0;
//		p[i] = 0;
//	}
//	free(p);
//	p = NULL;
//	return 0;
//}

//3.3对非动态开辟内存的空间内存释放
//int main()
//{
//	int a = 0;
//	int* p = &a;
//
//	free(p);//错误
//	p = NULL;
//	return 0;
//}

//3.4使用free释放动态开辟内存的一部分
//int main()
//{
//	int* p = (int*)malloc(100);
//	if (NULL == p)
//	{
//		return 1;
//	}
//	int i = 0;
//	for (i = 0; i < 25; i++)
//	{
//		*p = 1;
//		p++;
//	}
//
//	free(p);
//	p = NULL;
//	return 0;
//}

//3.4对同一块动态内存多次释放

//int main()
//{
//	int* p = (int*)malloc(100);
//	if (NULL == p)
//	{
//		return 1;
//	}
//
//	free(p);
//
//
//	free(p);
//
//	return 0;
//}

//3.5动态内存忘记释放(内存泄露)

//void test()
//{
//	int* p = (int*)malloc(100);
//	if (NULL == p)
//	{
//		return 1;
//	}
//	//使用
//	free(p);
//	p = NULL;
//}
//int main()
//{
//	test();
//	//
//
//	return 0;
//}


//int main()
//{
//	while (1)
//	{
//		malloc(1);
//	}
//	return 0;
//}

