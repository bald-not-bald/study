#define _CRT_SECURE_NO_WARNINGS
#include<stdio.h>

//二级指针
//int main()
//{
//	int a = 10;
//	int* pa = &a;
//	*pa = 20;//a = 20
//
//
//	return 0;
//}


//int main()
//{
//	int a = 10;
//	int * pa = &a;
//	int* * ppa = &pa;//二级指针变量
//	printf("%d\n", **ppa);
//
//	//int** * pppa = &ppa;
//	return 0;
//}

//指针数组
//指针数组是数组，是存放指针变量的数组


//1 2 3 4 5 
//int main()
//{
//	int arr[5] = { 1,2,3,4,5 };
//	int a = 10;
//	int b = 20;
//	int c = 30;
//	int d = 40;
//	int e = 50;
//
//	int* arr1[5] = { &a,&b,&c,&d,&e };
//	int i = 0;
//	for (i = 0; i < 5; i++)
//	{
//		printf("%d ", * (arr1[i]));
//	}
//
//
//	return 0;
//}


//int main()
//{
//	int a[] = { 1,2,3,4 };
//	int b[] = { 1,2,3,4 };
//	int c[] = { 1,2,3,4 };
//	int* arr[3] = { a,b,c };
//	int i = 0;
//	for (i = 0; i < 3; i++)
//	{
//		int j = 0;
//		for (j = 0; j < 4; j++)
//		{
//			printf("%d ", arr[i][j]);
//		}
//		printf("\n");
//	}
//
//
//	return 0;
//}


