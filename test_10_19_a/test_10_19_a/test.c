#define _CRT_SECURE_NO_WARNINGS


#include <stdio.h>
#include <string.h>

//
//自定义函数
//
//int max(int x, int y)
//{
//	return(x > y ? x : y);
//}
//
//
//int main()
//{
//	int a = 0;
//	int b = 0;
//	scanf("%d %d", &a, &b);
//
//	//求较大值
//	//函数的调用
//	int m = max(a, b);
//	printf("%d\n", m);
//
//	return 0;
//}

//写一个函数可以交换两个整形变量的内容


//形式参数
//void Swap(int x, int y)
//{
//	int z = 0;
//	z = x;
//	x = y;
//	y = z;
//}
////当实参传递给行参的是后，行参是实参的一份临时拷贝
////对行参的修改不会影响实参
//int main()
//{
//	int a = 0;
//	int b = 0;
//	scanf("%d %d", &a, &b);
	//交换
	//printf("交换前:a = %d b = %d", a, b);
	////a和b叫实参
	//Swap(a, b);
	//printf("交换后:a = %d b = %d", a, b);
//
//
//	return 0;
//}

//void Swap(int* px, int* py)
//{
//	int z = *px;
//	*px = *py;
//	*py = z;
//}

//int Add(int x, int y)
//{
//	return x + y;
//}

//int main()
//{
//	int a = 0;
//	int b = 0;
//	scanf("%d %d", &a, &b);
//
//	//int c = Add(a, b);
//	//printf("%d\n", c);
//
//	//交换
//	printf("交换前:a = %d b = %d\n", a, b);
//
//	Swap(&a, &b);
//	printf("交换后:a = %d b = %d\n", a, b);
//
//	return 0;
//}

//函数的调用
// 1.传值调用
// 函数的形参和实参分别占用不同的内存块，对行参的修改不会影响实参
// 2.传址调用
//是把函数外部创建变量的内存地址传递给函数参数的一种调用函数的方法
//这种传参方法可以让函数和函数外边的变量建立起真正的联系，也就是函数内部可以直接操作函数外部的变量

//写一个函数可以判断一个数是不是素数


//打印100-200之间的素数

//#include<math.h>
//
//int is_prime(int n)
//{
//	int j = 0;
//	for (j = 2; j <= sqrt(n); j++)
//	{
//		if (n % j == 0)
//		{
//			return 0;
//		}
//	}
//	return 1;
//}
//
//
//int main()
//{
//	int i = 0;
//	for (i = 101; i <= 200; i+=2)
//	{
//		//判断1是否为素数
//		//是素数就打印
//		//拿2-i-1之间的数字去试除i
//		if (is_prime(i))
//			printf("%d ", i);
//	}
//	return 0;
//}


//写一个函数判断一年是不是闰年
//打印1000-2000年之间的闰年
//闰年判断的规则
// 1.能被4整除，并且不能被100整除是闰年
//2.能被400整除是闰年
//int is_leap_year(int y)
//{
//	if (((y % 4 == 0) && (y % 100 != 0)) || (y % 400 == 0))
//		return 1;
//	else
//		return 0;
//}
//
//int main()
//{
//	int year = 0;
//	for (year = 1000; year <= 2000; year++)
//	{
//		////判断是不是闰年
//		//if (year % 4 == 0)
//		//{
//		//	if (year % 100 != 0)
//		//	{
//		//		printf("%d ", year);
//		//	}
//		//}
//		//if (year % 400 == 0)
//		//{
//		//	printf("%d ", year);
//		//}
//		/*if (((year % 4 == 0) && (year % 100 != 0)) || (year % 400 == 0))
//		{
//			printf("%d ", year);
//		}*/
//		if (is_leap_year(year))
//		{
//			printf("%d ", year);
//		}
//	}
//
//	return 0;
//}

//写一个函数，实现一个整形有序数组的二分查找
int binary_search(int arr[], int k, int sz)
{
	int left = 0;
	int right = sz - 1;
	while (left <= right)
	{
		int mid = left + (right - left) / 2;
		if (arr[mid] < k)
		{
			left = mid + 1;
		}
		else if (arr[mid] > k)
		{
			right = mid - 1;
		}
		else
		{
			return mid;//找到了返回下标
		}
	}
	return -1;//找不到
}


int main()
{
	int arr[] = { 1,2,3,4,5,6,7,8,9,10 };
	int k = 7;
	int sz = sizeof(arr) / sizeof(arr[0]);


	//找到了返回下标
	//找不到返回-1
	int ret = binary_search(arr, k,sz);
	if (ret == -1)
	{
		printf("找不到\n");
	}
	else
	{
		printf("找到了，下标是:%d\n", ret);
	}

	return 0;
}



