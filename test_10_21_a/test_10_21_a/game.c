#define _CRT_SECURE_NO_WARNINGS
#include"game.h"


void InitBoard(char board[ROWS][COLS], int rows, int cols,char set)
{
	int i = 0;
	int j = 0;
	for (i = 0; i < rows; i++)
	{
		for (j = 0; j < cols; j++)
		{
			board[i][j] = set;
		}
	}
}


void DisplayBoard(char board[ROWS][COLS], int row, int col)
{
	int i = 0;
	int j = 0;
	printf("------扫雷游戏------\n");
	for (j = 0; j <= col; j++)
	{
		printf("%d ", j);
	}
	printf("\n");
	for (i = 1; i <= row; i++)
	{
		printf("%d ", i);
		for (j = 1; j <= col; j++)
		{
			printf("%c ", board[i][j]);
		}
		printf("\n");
	}
	printf("------扫雷游戏------\n");
}


void SetMine(char board[ROWS][COLS], int row, int col)
{
	int count = EASY_COUNT;
	while (count)
	{
		int x = rand() % row + 1;
		int y = rand() % col + 1;

		if (board[x][y] == '0')
		{
			board[x][y] = '1';
			count--;
		}

	}
}

int get_mine_count(char board[ROWS][COLS], int x, int y)
{
	return (board[x - 1][y] +
		board[x - 1][y - 1] +
		board[x][y - 1] +
		board[x + 1][y - 1] +
		board[x + 1][y] +
		board[x + 1][y + 1] +
		board[x][y + 1] +
		board[x - 1][y + 1] - 8 * '0');
}


void FindMine(char mine[ROWS][COLS], char show[ROWS][COLS], int row, int col)
{
	int x = 0;
	int y = 0;
	int flag = 0;
	int note = 0;
	int win = 0;//找到非零的个数
	int* p = &win;
	while (1)
	{
		printf("请选择要:0.排查雷  1.标记雷\n");
		scanf("%d",&note);
		switch (note)
		{
		case 0:
					opt:
			printf("请输入要排查雷的坐标:>");
			scanf("%d %d", &x, &y);
			if (x >= 1 && x <= row & y >= 1 && y <= col)
			{
				if (mine[x][y] == '1')
				{
					printf("很遗憾，你被炸死了\n");
					DisplayBoard(mine, ROW, COL);
					do
					{
						printf("输入0进行下一步操作:");
						scanf("%d", &flag);
						if (flag == 0)
							goto exit;
						else
						{
							printf("输入错误，请重新输入\n");
							DisplayBoard(show, ROW, COL);
						}
					} while (flag);
					break;
				}
				else
				{
					//用递归实现自动排查周围无雷的格子
					get_mine(mine, show, x, y, row, col, p);
					//显示排查的信息
					DisplayBoard(show, ROW, COL);
				}
			}
			else
			{
				printf("输入坐标非法，请重新输入\n");
				DisplayBoard(show, ROW, COL);
					goto opt;
			}
			break;
		case 1:
			Setnote(mine, show, x, y, row, col);
			break;
		}
		if (win == row * col - EASY_COUNT|| win_s == EASY_COUNT)
		{
			printf("恭喜你，排雷成功\n");
			win = 0;
			win_s = 0;
				exit:
			break;
		}

	}

}

void get_mine(char mine[ROWS][COLS], char show[ROWS][COLS], int x, int y, int row, int col,int* p)
{
	if (x > 0 && x <= row && y > 0 && y <= col)
	{
		int count = get_mine_count(mine, x, y);
		if (count == 0)
		{
			(*p++);
			int a, b;
			show[x][y] = '0';
			for (a = -1; a <= 1; a++)
			{
				for (b = -1; b <= 1; b++)
				{
					if (show[x + a][y + b] == '*')
						get_mine(mine, show, x + a, y + b, row, col, p);
				}
			}
		}
		else
		{
			(*p)++;
			show[x][y] = count + '0';
		}
	}

}



//标记雷的函数定义
void Setnote(char mine[ROWS][COLS], char show[ROWS][COLS], int x, int y, int row, int col)
{
	while (1)
	{
		printf("请输入要标记的雷的坐标:");
		scanf("%d %d", &x, &y);
		if (x >= 1 && x <= row && y >= 1 && y <= col && show[x][y] != '0' && show[x][y] != '1')
		{
			show[x][y] = '@';
			if (mine[x][y] == '1')
				win_s++;
			break;
		}
		else
		{
			printf("输入坐标不合法，请重新输入\n");
			DisplayBoard(show, row, col);
		}
	}
	DisplayBoard(show, row, col);
}

