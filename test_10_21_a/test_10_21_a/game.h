#pragma once


#include<stdio.h>
#include<time.h>
#include<stdlib.h>

#define ROW 9
#define COL 9

#define ROWS ROW+2
#define COLS COL+2

#define EASY_COUNT 10

int win_s;


//初始化数组
void InitBoard(char board[ROWS][COLS], int rows, int cols,char set);

//打印界面
void DisplayBoard(char board[ROWS][COLS],int row,int col);

//设置雷
void SetMine(char board[ROWS][COLS], int row, int col);

//排查雷
void FindMine(char mine[ROWS][COLS],char show[ROWS][COLS],int row, int col);

//优化排查雷
void get_mine(char mine[ROWS][COLS], char show[ROWS][COLS], int x, int y, int row, int col, int* p);

//标记雷
void Setnote(char mine[ROWS][COLS], char show[ROWS][COLS], int x, int y, int row, int col);





