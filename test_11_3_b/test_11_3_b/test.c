#define _CRT_SECURE_NO_WARNINGS

#include<stdio.h>
#include<string.h>
//使用qsort排序结构体
//struct Stu
//{
//	char name[20];
//	int age;
//};
//
////按照学生年龄排序
//int cmp_stu_by_age(const void* e1, const void* e2)
//{
//	return ((struct Stu*)e1)->age - ((struct Stu*)e2)->age;
//}
//int cmp_stu_by_name(const void* e1, const void* e2)
//{
//	return strcmp(((struct Stu*)e1)->name, ((struct Stu*)e2)->name);
//}
//
//void test2()
//{
//	struct Stu s[3] = { {"zhangsan",30},{"lisi",20},{"wangjie",35} };
//	int sz = sizeof(s) / sizeof(s[0]);
//	//qsort(s, 3, sizeof(s[0]), cmp_stu_by_age);
//	qsort(s, 3, sizeof(s[0]), cmp_stu_by_name);
//}
//int main()
//{
//	test2();
//
//	return 0;
//}

void Swap(char* buf1, char* buf2, int width)
{
	int i = 0;
	for (i = 0; i < width; i++)
	{
		char tmp = *buf1;
		*buf1 = *buf2;
		*buf2 = tmp;
		buf1++;
		buf2++;
	}
}
int cmp_int(const void* e1, const void* e2)
{
	return *(int*)e1 - *(int*)e2;
}
void bubble_sort(void* base,size_t sz,size_t width, int(*cmp)(const void*e1,const void*e2))
{
	int i = 0;
	for (i = 0; i < sz - 1; i++)
	{
		//一趟冒泡排序
		int j = 0;
		for (j = 0; j < sz - 1 - i; j++)
		{
			if (cmp((char*)base+j*width,(char*)base+(j+1)*width)>0)
			{
				//交换
				Swap((char*)base + j * width, (char*)base + (j + 1) * width, width);
			}

		}
		
	}
}


void test3()
{
	int arr[] = { 9,8,7,6,5,4,3,2,1,0 };
	int sz = sizeof(arr) / sizeof(arr[0]);

	bubble_sort(arr, sz, sizeof(arr[0]), cmp_int);
	int i = 0;
	for (i = 0; i < sz; i++)
	{
		printf("%d ", arr[i]);
	}
}

//改造冒泡排序，使得它可以对任意数排序
int main()
{
	test3();

	return 0;
}
