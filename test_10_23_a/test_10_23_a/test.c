#define _CRT_SECURE_NO_WARNINGS

#include<stdio.h>

//int main()
//{
//	//int a = 10;
//	//printf("%p\n", &a);
//	//int* pa = &a;
//
//	//char ch = 'w';
//	//char* pc = &ch;
//
//	//char arr[10] = { 0 };
//	//char* p2 = arr;
//	//char* p3 = &arr[0];
//
//	char* p = "abcdef";
//	printf("%p\n", p);
//	printf("%c\n", *p);
//
//	return 0;
//}


//int main()
//{
//	int a = 10;
//	int* pa = &a;
//
//
//	return 0;
//}

//int main()
//{
//	int a, b, c;
//	a = 5;
//	c = ++a;
//	b = ++c, c++, ++a, a++;
//	b += a++ + c;
//	printf("a = %d b = %d c = %d\n", a, b, c);
//	return 0;
//}

//int main()
//{
//	int count = 0;
//	int a = 0;
//	scanf("%d", &a);
//	int i = 0;
//	for (i = 0; i < 32; i++)
//	{
//		if (a & 1 << i)
//			count++;
//	}
//	printf("%d\n", count);
//	return 0;
//}

//二进制位中1的个数
//int Numberof1(int n)
//{
//	int count = 0;
//	while (n)
//	{
//		n = n & (n - 1);
//		count++;
//	}
//	return count;
//}
//int main()
//{
//	int a = 0;
//	scanf("%d", &a);
//	int ret = Numberof1(a);
//	printf("%d", ret);
//
//	return 0;
//}

//写一个代码，判断n是不是2的k次方？
//2的k次方的数字，2进制的标识中只有一个1


//int isPowerOfTwo(int n) {
//    if (n <= 0) {
//        return 0;
//    }
//    return (n & (n - 1)) == 0;
//}
//
//int main() {
//    int n;
//    printf("请输入一个整数：");
//    scanf("%d", &n);
//    if (isPowerOfTwo(n)) {
//        printf("%d 是 2 的 k 次方。\n", n);
//    }
//    else {
//        printf("%d 不是 2 的 k 次方。\n", n);
//    }
//    return 0;
//}

//获取一个整数的二进制中所有的奇数位和偶数位,分别打印二进制数列
//void Print(int n)
//{
//	int i = 0;
//	printf("奇数位:");
//	for (i = 30; i >= 0; i -= 2)
//	{
//		printf("%d ", (n >> i)&1);
//	}
//	printf("\n");
//
//	printf("偶数位:");
//	for (i = 31; i >= 0; i -= 2)
//	{
//		printf("%d ", (n >> i) & 1);
//	}
//	printf("\n");
//
//}
//
//int main()
//{
//	int a = 0;
//	scanf("%d", &a);
//
//	Print(a);//打印n的二进制中所有奇数位和所有偶数位
//	return 0;
//}

//求两个int（32位）整数m和n的二进制表达中，有多少个位（bit）不同？
//int count_diff_one(int m, int n)
//{
//	int i = 0;
//	int count = 0;
//	for (i = 0; i < 32; i++)
//	{
//		if (((m >> i) & 1) != ((n >> i) & 1))
//		{
//			count++;
//		}
//	}
//	return count;
//}
//异或操作符
//相同为0，相异为1
//int count_diff_one(int m, int n)
//{
//	int tmp = m ^ n;
//	int count = 0;
//	while (tmp)
//	{
//		tmp = tmp & (tmp - 1);
//		count++;
//	}
//}
//
//int main()
//{
//	int m = 0;
//	int n = 0;
//	scanf("%d %d", &m, &n);
//	int ret = count_diff_one(m, n);
//	printf("%d", ret);
//	return 0;
//}

//走台阶
//小乐乐上课需要走n阶台阶，因为他的腿比较长，所以每次可以选择走一阶或者走两阶，那么他一共有多少种走法
//递归实现
//int Fib(int n)
//{
//	if (n <= 2)
//	{
//		return n;
//	}
//	else
//		return Fib(n - 1) + Fib(n - 2);
//}
//int main()
//{
//	int n = 0;
//	scanf("%d", &n);
//	int ret = Fib(n);
//
//	printf("%d", ret);
//	return 0;
//}


//获得月份天数

//int get_days_of_month(y, m)
//{
//	int days[13] = { 0,31,28,31,30,31,30,31,31,30,31,30,31 };
//	//                 1  2  3  4  5  6  7  8  9  10 11 12
//	int day = days[m];
//	if ((m == 2) && (((y % 4 == 0) && (y & 100 != 0)) || (y % 400 == 0)))
//	{
//		day++;
//	}
//	return day;
//}
//int main()
//{
//	int y = 0;
//	int m = 0;
//	while (scanf("%d %d", &y, &m) == 2)
//	{
//		int ret = get_days_of_month(y, m);
//		printf("%d\n", ret);
//	}
//
//	return 0;
//}
// 


//int i;
//int main()
//{
//	i--;
//	if (i > sizeof(i))//sizeof的计算结构是无符号的整数
//	{
//		printf(">\n");
//	}
//	else
//	{
//		printf("<\n");
//	}
//	return 0;
//}

//矩阵计算
//int main()
//{
//	int n = 0;
//	int m = 0;
//	scanf("%d %d", &n, &m);
//	int j = 0;
//	int i = 0;
//	int tmp = 0;
//	int sum = 0;
//	for (i = 0; i < n; i++)
//	{
//		for (j = 0; j < m; j++)
//		{
//			scanf("%d", &tmp);
//			if(tmp > 0)
//				sum += tmp;
//		}
//	}
//	printf("%d\n", sum);
//}


//输入一个正整数n（1<=n<=10^9
//输出一行，为正整数n的6进制的结果
//void Print(int n)
//{
//	if (n > 5)
//	{
//		Print(n / 6);
//	}
//	printf("%d", n%6);
//}
//int main()
//{
//	int n = 0;
//	scanf("%d", &n);
//	Print(n);
//
//	//int arr[20] = { 0 };
//	//int n = 0;
//	//scanf("%d", &n);
//	//int i = 0;
//	//while (n)
//	//{
//	//	arr[i++] = n % 6;
//	//	n /= 6;
//
//	//}
//	//for (--i; i >= 0; i--)
//	//{
//	//	printf("%d", arr[i]);
//	//}
//	return 0;
//}


//实现矩阵的转置          变长数组需要支持c99的编译器
//int main()
//{
//	int n = 0;
//	int m = 0;
//	scanf("%d %d", &n, &m);
//	//int arr[n][m]];
//	int j = 0;
//	int i = 0;
//	for (i = 0; i < n; i++)
//	{
//		for (j = 0; i < m; j++)
//		{
//			scanf("%d", &arr[i][j]);
//		}
//	}
//	for (i = 0; i < m; i++)
//	{
//		for (j = 0; i < n; j++)
//		{
//			printf("%d", &arr[j][i]);
//		}
//		printf("\n");
//	}
//	return 0;
//}


//序列种删除指定数字
//int main()
//{
//	int n = 0;
//	scnaf("%d", &n);
//
//	int arr[50] = { 0 };
//	int i = 0;
//	for (i = 0; i < 50; i++);
//	{
//		scanf("%d", &arr[i]);
//	}
//
//	int del = 0;
//	scnaf("%d", &del);
//	for (i = 0; i < 50; i++)
//	{
//		if (arr[i] == del)
//			arr[i] = 0;
//	}
//	for (i = 0; i < 50; i++)
//	{
//		if (arr[i] != del)
//			printf("%d", arr[i]);
//	}
//
//
//	return 0;
//}





