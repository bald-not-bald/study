#define _CRT_SECURE_NO_WARNINGS


//1.字面常量
// 2.const修饰的常变量
// 3.define定义的标识符常量
//4.枚举常量

#include<stdio.h>
//int main()
//{
//
//	//30;
//	//3.14;
//	//'w';//字符
//	//'abc';
//	//const int a = 10;//在C语言中，const修饰的a，本质是变量，但是不能直接修改，有常量的属性。
//	//a = 20;
//	//printf("%d\n", a);//20
//
//	const int n = 10;
//	int arr[10] = { 0 };
//
//
//	return 0;
//}


//#define MAX 100
//#define STR "abcdef"
//#include<stdio.h>
//int main()
//{
//	printf("%d\n", MAX);
//	int a = MAX;
//	printf("%d\n", a);
//	printf("%s\n", STR);
//	return 0;
//}

//枚举常量
//enum Color
//{
//	//枚举常量
//	RED,
//	GREEN,
//	BLUE
//};
////性别
//enum  Sex
//{
//	MALE,
//	FEMALE,
//	SECRET
//};
//
//int main()
//{
//	//三原色
//	//red green blue
//
//	int num = 10;
//	enum Color c = RED;
//
//
//
//	return 0;
//}

#include <string.h>
//int main()
//{
//	//
//	//char 字符类型
//	//'a';
//	/*char ch = 'w';*/
//	//字符串
//	//C语言里没有字符串类型
//	/*'abcdef';*/
//	char arr1[] = "abcdef";//7
//	char arr2[] = { 'a','b','c','d','e','f','\0'};
//
//
//	printf("%d\n", strlen(arr1));//6
//	printf("%d\n", strlen(arr2));
//
//	//int len = strlen("abc");//求字符串长度的一个函数，string length 头文件string.h
//	//printf("%d\n", len);
//
//	/*printf("%s\n", arr);
//	printf("%s\n", arr2);*/
//
//	return 0;
//}

//int main()
//{
//	printf("abc\n");//\n代表换行
//
//	return 0;
//}


//int main()
//{
//	printf("abc\0def");
//
//	return 0;
//}

//转义字符
//三字母词
//??) -->]
//??( -->[

//%d - 打印整型
//%c - 打印字符
//%s - 打印字符串
//%f - 打印float类型的数据
//%lf - 打印double类型的数据
//%zu - 打印sizeof的返回值


//int main()
//{
	//printf("%s\n", "(are you ok\?\?)");

	//printf("%c\n", '\'');
	//printf("abcdef\n");
	//printf("\"");

	//printf("abcd\\0ef");

	//printf("c:\\test\\test.c");'\\'防止'\'转义

	//printf("\a");警告字符，电脑响一下
	//printf("abc\ndef");换行

	//printf("%c\n", '\130');//8进制数字
	//printf("%c\n", '\x60');//16进制数字
	//printf("%d\n", strlen("qwer t"));//
	//printf("%d\n", strlen("c:\test\628\test.c"));


	//return 0;
//}

//注释
//
/*   C语言的注释风格


int main()
{
	//int a = 10;

	//创建指针变量p，并赋值位NULL
	int* p = NULL;

	return 0;
}
*/

//int main()
//{
//	//int a = 10;
//	//C++的注释风格
//
//	return 0;
//}


//1.注释可以梳理
// 2.对复杂的代码进行解释
// 3.写代码的时候写注释，是帮助自己，帮助别人
//


//int main()
//{
//	//接受输入
//
//	//处理数据
//
//	//输出
//
//	return 0;
//}

//
//



//int main()
//{
//	int input = 0;
//	printf("加入比特\n");
//	printf("要好好学习吗(1/0)?\n");
//	scanf("%d\n", &input);
//	if (input == 1)
//	{
//		printf("好offer\n");
//	}
//	else
//	{
//		printf("卖红薯\n");
//	}
//
//	return 0;
//}


//while循环的例子
//2w行有效代码的积累
//int main()
//{
//	int line = 0;
//	printf("加入比特\n");
//
//
//	while (line < 20000)
//	{
//		printf("写代码\n",line);
//		line++;
//	}
//	if (line >= 20000)
//	{
//		printf("好offer\n");
//	}
//	else
//	{
//		printf("继续加油\n");
//	}
//	return 0;
//}

//求两个任意整数的和


//int Add(int x, int y)
//{
//	int z = 0;
//	z = x + y;
//	return z;
//}


//int Add(int x, int y)
//{
//	return (x+y);
//}
//
//int main()
//{
//	int n1 = 0;
//	int n2 = 0;
//	//输入
//	scanf("%d %d", &n1, &n2);
//	//求和
//	//int sum = n1 + n2;
//	int sum = Add(n1, n2);
//	int t1 = 100;
//	int t2 = 300;
//	int ret = Add(t1, t2);
//
//	//打印
//	printf("%d\n", sum);
//
//	return 0;
//}

//数组

//int main()
//{
//	//int a = 10;
//	//int b = 11;
//	//int c = 12;
//	//数组
//	int arr[10] = {0,1,2,3,4,5,6,7,8,9};
//	char ch[5];
//	double d[30];
//
//	//printf("%d\n", arr[8]);
//
//	while (i < 10)
//	{
//		printf("%d", arr[i]);
//		i = i + 1;
//	}
//	return 0;
//}





