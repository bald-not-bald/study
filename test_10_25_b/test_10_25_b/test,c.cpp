#define _CRT_SECURE_NO_WARNINGS

#include<stdio.h>

//结构体
//结构是一些值的集合，这些值称为成员变量，结构的每个成员可以是不同类型的变量
//

//struct Stu
//{
//	//结构体成员
//	char name[20];
//	int age;
//	char sex[10];
//	float score;
//}s4,s5;//s4,s5也是结构体变量 - 全局变量
//
//
//int main()
//{
//	struct Stu s1 = { "zhangsan",20,"男",95.5f };//结构体变量 - 局部变量
//	struct Stu s2 = {"wangcai",4,"gou",59.5f};
//	printf("%s %d %s %.lf\n", s1.name, s1.age, s1.sex, s1.score);
//	printf("%s %d %s %.lf\n", s2.name,s2.age,s2.sex,s2.score);
//	return 0;
//}
//struct S
//{
//	int a;
//	char c;
//};
//
//struct P
//{
//	double d;
//	struct S s;
//	float f;
//};
//
//int main()
//{
//	struct P p = { 5.5,{100,'b'},3.14f };
//
//	printf("%d %c\n", p.s.a, p.s.c);
//
//	return 0;
//}


//struct S
//{
//	int a;
//	char c;
//};
//
//struct P
//{
//	double d;
//	struct S s;
//	float f;
//};
//void Print1(struct P sp)
//{
//	//结构体变量.成员
//	printf("%lf %d %c %f\n", sp.d, sp.s.a, sp.s.c, sp.f);
//}
//void Print2(struct P* p1)
//{
//	printf("%lf %d %c %f\n", (*p1).d, (*p1).s.a, (*p1).s.c, (*p1).f);
//	//结构体指针->成员
//	printf("%lf %d %c %f\n", p1->d, p1->s.a, p1->s.c, p1->f);
//}
//int main()
//{
//	struct P p = { 5.5,{100,'b'},3.14f };
//
//	//printf("%d %c\n", p.s.a, p.s.c);
//	Print1(p);//传值调用
//	Print2(&p);//传址调用
//
//	return 0;
//}
//结构体传参的时候，最好传递结构体的地址
#include<string.h>

void reverse(char* str)
{
	int left = 0;
	int right = strlen(str) - 1;
	while (left < right)
	{
		char tmp = *(str + left);
		*(str + left) = *(str + right);
		*(str + right) =  tmp;
		left++;
		right--;
	}

}

int main()
{
	char arr[10000] = { 0 };
	//gets(arr);
	//scanf("%[^\n]", arr);
	reverse(arr);
	printf("%s\n", arr);
	return 0;
}

